<div id="contenido">
    <!-- InstanceBeginEditable name="contenido" -->
    <div class="titulo">
        <div class="logo_pequeno">
            <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
        </div>
        <div class="txt_titulo">
            Noticias
        </div>
    </div>
    
    <div id="contenido_nosotros">
        <h1><?php echo $noticia->titulo; ?></h1>
        <span><?php echo (strlen($noticia->subtitulo) > 0)? $noticia->subtitulo : '&nbsp;'; ?></span>
        <br />
        <br />
        <br />
    </div>
    
    <div class="fecha_seminario_noticia">
        <?php echo reformat_date($noticia->fecha_creacion, 'd-m-Y H:i'); ?>
    </div>
    <div class="contenedor_noticia">
        <?php
        
            if(strlen($noticia->imagen) > 0)
            {
                echo "<img src='/uploads/img/noticias/destacadas/{$noticia->imagen}' alt='{$noticia->titulo}'>";
            }
        ?>
        
        <p>
            <?php echo $noticia->contenido; ?>
        </p>
        
    </div>

    <div class="enumerador">
        <a href="/">&lt;&lt; Volver al listado</a> -
        
        <?php
            if(isset($prev_next['anterior']->titulo))
            {
                $link = "/noticias/{$prev_next['anterior']->id}-" . slugify($prev_next['anterior']->titulo);
                ?>
                <a href="<?php echo $link; ?>">&lt;Anterior</a> -
                <?php
            }
            
            if(isset($prev_next['siguiente']->titulo))
            {
                $link = "/noticias/{$prev_next['siguiente']->id}-" . slugify($prev_next['siguiente']->titulo);
                ?>
                <a href="<?php echo $link; ?>">Siguiente &gt;</a>
                <?php
            }
        ?>
    </div>
    <!-- Esto deber�a ir arriba como breadcum
    -->
    <!-- InstanceEndEditable -->
</div>