<!-- start: index contenido -->
<div id="index_contenido">

    <img src="/assets/site/img/logo_chico_contenidos.png" />
    <section>NOTICIAS</section>
        
    <div class="contenedor_noticias">
        <?php
        
            foreach($noticias as $noticia)
            {
                $link = "/noticias/{$noticia->id}-" . slugify($noticia->titulo);
                $imagen = '/uploads/img/noticias/destacadas/' . ((strlen($noticia->imagen) > 0)? $noticia->imagen : '_destacada_default.small.jpg');
                ?>
                <div class="noticia_iz">
                    <img src="<?php echo $imagen; ?>" /> 
                    <!--<h1><a href="<?php echo $link; ?>"><?php echo $noticia->titulo; ?></a></h1>-->
                    <h1><?php echo $noticia->titulo; ?></h1>
                    <p><?php echo plain_text($noticia->contenido); ?>...    </p>
                    <a href="<?php echo $link; ?>">...Leer M&aacute;s</a>
                </div>
                <?php
            }
        
        ?>
        
    </div>
    <br><br><br>
    <div class="enumerador">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    
    <?php
        $this->load->view('public/_colaboradores');
    ?>

</div>
<!-- end: index contenido -->