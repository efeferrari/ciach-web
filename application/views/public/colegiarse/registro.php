<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    
    </div>
    <div class="txt_titulo">
        colegiarse
    </div>
</div>
    <div id="contenido_nosotros">
        <h1>&iquest;por qu&eacute; colegiarse en el chiach</h1>
        <span>&iexcl;encuentra tu oportunidad!</span><br /><br /><br />
    </div>
<div id="contenedor_colegiarse">
    <p>
        La pertenencia al Colegio de Ingenieros en Alimentos de Chile A.G. CIACH contribuye a dignificar y fortalecer la profesi&oacute;n, ya que sus 
        colegiados aceptan asumir voluntariamente la supervisi&oacute;n &eacute;tica de sus pares, lo que constituye una garant&iacute;a de justo ejercicio de la 
        carrera, lo que agrega altos est&aacute;ndares profesionales para sus labores.<br />

        El ser colegiado permite acceder a constante perfeccionamiento profesional al posibilitar a los ingenieros asociados a participar, en 
        condiciones preferenciales, en seminarios, charlas, congresos, diplomados y otras actividades acad&eacute;micas organizadas y/o patrocinadas 
        por el Colegio de Ingenieros en Alimentos. Los colegiados pueden participar en las comisiones program&aacute;ticas del Colegio.<br />

        Ser colegiado le permite requerir amparo profesional del Colegio de Ingenieros en Alimentos cuando exista una arbitrariedad al ejercicio 
        profesional.
        <br />
        <br />
        El Colegio formula declaraciones p&uacute;blicas, peticiones y/o pronunciamientos acerca de asuntos de inter&eacute;s profesional, fortalecimiento 
        del gremio, para defender la salud p&uacute;blica y la seguridad alimentaria, as&iacute; como tambi&eacute;n difusi&oacute;n de informaciones de organizaciones 
        nacionales e internacionales dedicados a los alimentos.
        <br />
        Los ingenieros pueden incorporarse al Colegio, a trav&eacute;s del sistema de inscripci&oacute;n en l&iacute;nea, completando los antecedentes requeridos 
        en todos los campos.
        <br />
        <br />
        El Colegio debe verificar los antecedentes para concretar la colegiatura. &eacute;sta se informa, a trav&eacute;s de un correo e-mail de confirmaci&oacute;n 
        de parte del Colegio en el cual se acepta la postulaci&oacute;n.
    </p>
    
    <form action='/colegiarse/save/' method='post' enctype="multipart/form-data" id="colegiarse">
        <table width="680" border="0">
              <tr>
                <td><label for='usuario_nombre'>Nombres</label></td>
                <td><input id='usuario_nombre' name="usuario[nombre]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_apellido_paterno'>Apellido paterno</label></td>
                <td><input id='usuario_apellido_paterno' name="colegiado[apellido_paterno]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_apellido_materno'>Apellido materno</label></td>
                <td><input id='usuario_apellido_materno' name="colegiado[apellido_materno]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_correo'>Correo eletr&oacute;nico personal</label></td>
                <td><input id='usuario_correo' name="usuario[correo]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_clave'>Contrase&ntilde;a</label></td>
                <td><input id='usuario_clave' name="usuario[clave]" type="password" /></td>
              </tr>
              <tr>
                <td><label for='usuario_run'>R.U.N.</label></td>
                <td><input id='usuario_run' name="colegiado[run]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_fecha_nacimiento'>Fecha de nacimiento</label></td>
                <td><input id='usuario_fecha_nacimiento' name="colegiado[fecha_nacimiento]" type="text" class='has_calendar' readonly /></td>
              </tr>
              <tr>
                <td><label for='usuario_nacionalidad'>Nacionalidad</label></td>
                <td><input id='usuario_nacionalidad' name="colegiado[nacionalidad]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_telefono'>Tel&eacute;fono</label></td>
                <td><input id='usuario_telefono' name="colegiado[telefono]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_ciudad_residencia'>Ciudad de residencia</label></td>
                <td><input id='usuario_ciudad_residencia' name="colegiado[ciudad_residencia]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_nombre_profesion'>Nombre de tu profesi&oacute;n</label></td>
                <td><input id='usuario_nombre_profesion' name="colegiado[nombre_profesion]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_universidad'>Universidad que te titulaste</label></td>
                <td><input id='usuario_universidad' name="colegiado[universidad]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_anno_titulacion'>A&ntilde;o de titulaci&oacute;n</label></td>
                <td><input id='usuario_anno_titulacion' name="colegiado[anno_titulacion]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_area_desempeno'>&Aacute;rea de desempe&ntilde;o actual</label></td>
                <td><input id='usuario_area_desempeno' name="colegiado[area_desempeno]" type="text" /></td>
              </tr>
              <tr>
                <td><label for='usuario_especialidad_id'>Especialidad</label></td>
                <td>
                    <select id='usuario_especialidad_id' name="colegiado[especialidad_id]">
                        <?php
                            foreach($especialidades as $especialidad)
                            {
                                echo "<option value='{$especialidad->id}'>{$especialidad->nombre}</option>";
                            }
                        ?>
                    </select>
                </td>
              </tr>
              
        </table>
            
        <div id="colegiarse_abajo_formulario">
            <p>Prometo respetar el <a href="/estatuto-codigo-etica">C&oacute;digo de &eacute;tica profesional del Colegio de Ingenieros en Alimentos</a> y someterme a su control &eacute;tico. *</p>
            <table>
                <tr>
                    <td><label for='acepta'>S&iacute;</label></td>
                    <td>
                        <input id='acepta' name="acepta" type="radio" value="1">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td align="center" rowspan="2">
                        <input type='submit' id='submit' name='Enviar' value='Enviar' style="border:none;color:#FFF; display:block; width:75px; height:25px; background:url(/assets/site/img/fondo_enviar.png) no-repeat">
                    </td>
                </tr>
            </table>
        </div>  
    
    </form>
    
</div>
<div id="colegiarse_aviso">
    <h2>IMPORTANTE</h2>
    <p>
        UNA VEZ COMPLETADO EL FORMULARIO ENVIAR CV ACTUALIZADO EN FORMATO PDF Y CERTIFICADO DE TITULO A contacto@ciach.cl
        y colingalimchile@gmail.com con copia a secretariaejecutiva@ciach.cl y finanzas@ciach.cl indicando en el asunto
        postulaci&oacute;n a colegiatura.
        <br />
        Estimado Ingeniero luego de confirmar su fecha de t&iacute;tulo comunicaremos a usted el proceso de
        inscripci&oacute;n v&iacute;a correo electr&oacute;nico.
    </p>
</div>