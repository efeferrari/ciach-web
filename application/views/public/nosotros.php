<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    
    </div>
    <div class="txt_titulo">
        nosotros
    </div>
</div>
<div id="contenido_nosotros">
  <h1>conoce nuestro equipo de igenieros</h1>
  <span>directiva</span><br /><br /><br />
    <img src="/assets/site/img/equipo.png" width="960px" height="200px" /><br />
  <h2>Presidente</h2>
  <h3>Gonzalo Letelier Salazar</h3>
  <h4>Email: gonzalo.letelier@ciach.cl - presidente@ciach.cl</h4>
    <p>
    Ingeniero de Alimentos titulado de la Pontificia Universidad Cat&oacute;lica de Valpara&iacute;so  <br />
Ingeniero con experiencia en el &aacute;rea de la biotecnolog&iacute;a, prote&iacute;nas, enzimas para la industria alimentaria. <br />Desarrolla actividades de 
comercializaci&oacute;n en Latinoam&eacute;rica en torno a la industria alimentaria.
    </p> <br /><br />
    
              <h2>VICEPRESIDENTE</h2>
  <h3>Paulo D&iacute;az Calder&oacute;n</h3>
  <h4>Email: vicepresidente@ciach.cl - paulo.diaz@ciach.cl</h4>
    <p>
    Es Ingeniero de Alimentos titulado de la Pontificia Universidad Cat&oacute;lica de Valpara&iacute;so<br />
Ingeniero con cuatro a&ntilde;os de experiencia laboral en empresa privada, dedicado actualmente a la investigaci&oacute;n en el &aacute;rea de estructuras y 
propiedades f&iacute;sicas de alimentos. 
    </p> <br /><br />
    
              <h2>SECRETARIA EJECUTIVA</h2>
  <h3>Olivia Henr&iacute;quez Amigo</h3>
  <h4>Email: secretariaejecutiva@ciach.cl - olivia.henriquez@ciach.cl
</h4>
    <p>
    Es Ingeniero en Alimentos titulado de la Universidad de Santiago de Chile.<br />
Ingeniero con experiencia en Aseguramiento de Calidad en la industria agroalimentaria, implementaci&oacute;n y operaci&oacute;n de normas.
    </p> <br /><br />
    
              <h2>DIRECTORA DE FINANZAS</h2>
  <h3>Ana Mar&iacute;a Castro Or&oacute;stegui</h3>
  <h4>Email: finanzas@ciach.cl - ana.castro@ciach.cl </h4>
    <p>
    Es Ingeniero en Industria Alimentaria titulada de la Universidad Tecnol&oacute;gica Metropolitana.<br /> 
Ingeniero con experiencia en sistemas de gesti&oacute;n e implementaci&oacute;n de sistemas de calidad.<br />
Actualmente se desarrolla en el &aacute;rea de especias y condimentos alimentarios.<br />
    </p> <br /><br />
                          <h2>	Director de Comunicaciones</h2>
  <h3>David Mora Aranda</h3>
  <h4>Email: comunicaciones@ciach.cl - david.mora@ciach.cl </h4>
    <p>Es Ingeniero de Alimentos titulado de la Pontificia Universidad Cat&oacute;lica de Valpara&iacute;so<br />
    Ingeniero de Alimentos con experiencia en el desarrollo y procesos de productos pecuarios, gestor de aseguramiento de la calidad. <br />
    Participa en actividad es de desarrollo local regional en la industria alimentaria.<br />
    Diplomado en Gesti&oacute;n de Calidad y Excelencia Organizacional de la Pontificia Universidad Cat&oacute;lica de Chile (PUC)<br />
    </p> <br /><br />
    
                          <h2>Asesor de Comunicaciones</h2>
  <h3>Alberto Valdenegro Andrade</h3>
  <h4>Email: alberto.valdenegro@ciach.cl - difusion@ciach.cl </h4>
    <p>
    Es Ingeniero en Alimentos Titulado de la Universidad de la Frontera<br />
    Ingeniero con experiencia en sistemas de gesti&oacute;n de calidad, y producci&oacute;n alimentaria, como tambi&eacute;n en el &aacute;rea de docencia y relator&iacute;as.<br />
    </p> <br /><br />
    
                          <h2>	Asesor de Innovaci&oacute;n</h2>
  <h3>oberto Coppellii</h3>
  <h4>Email: roberto.coppelli@ciach.cl - innovacion@ciach.cl </h4>
    <p>
    Ingeniero de Alimentos Pontificia Universidad Cat&oacute;lica de Valpara&iacute;so<br />
    Ingeniero con experiencia en sistemas de innovaci&oacute;n y gesti&oacute;n.<br />
    Doctor en gesti&oacute;n econ&oacute;mica, emprendimiento e innovaci&oacute;n, Universidad de Lleida<br />
    </p> <br /><br />

    

    
</div>	
<?php
    $this->load->view('public/_colaboradores');
?>
		