<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    
    </div>
    <div class="txt_titulo">
        Buscador de colegiados
    </div>
</div>
<div id="contenido_buscador">
    En esta secci&oacute;n, usted podr&aacute; buscar a los...<br /><br /><br />
    
    <form id='colegiados' name='form_buscar' action='/colegiados/buscar' method='post'>
        <p class="textos_buscador">Por Nombres</p>
        <input class="fondo_buscadores" name="buscador[nombres]" type="text" /><br /><br /><br />
        
        <p class="textos_buscador">por apellido paterno</p>
        <input class="fondo_buscadores" name="buscador[ap_paterno]" type="text" /><br /><br /><br />
        
        <p class="textos_buscador">Por apellido materno</p>
        <input class="fondo_buscadores" name="buscador[ap_materno]" type="text" /><br /><br /><br />
        
        <p class="textos_buscador">Por especialidad</p>
        <select id='usuario_especialidad_id' name="buscador[especialidad_id]">
            <option value=''>Todas</option>
            <?php
                foreach($especialidades as $especialidad)
                {
                    echo "<option value='{$especialidad->id}'>{$especialidad->nombre}</option>";
                }
            ?>
        </select>
        
        <br><br>
        
        <input id='boton_buscar' name="buscar" type="submit" value="Buscar" /><br /><br /><br />
    </form>
    
</div>
<div id='search_result'>
</div>