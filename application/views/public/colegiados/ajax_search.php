<div id="contenido_resultados">
    Resultado de b&uacute;squeda:<br /><br />
    <!--Usted busc&oacute;: <span>asd</span>-->
    
    <div id="barra_resultados">
        <?php
            if($search_count == 0)
            {
                ?>Sin aciertos<?php
            } elseif($search_count == 1)
            {
                ?>Un acierto<?php
            } else
            {
                echo $search_count; ?> acierto<?php
            }
        ?>
    </div><br>
    <?php
    
        if($search_count > 0)
        {
            foreach($colegiados as $colegiado)
            {
                ?>
                <div class="lista_resultados">
                    <div class="fecha_inscrito">
                        Inscrito el: <?php echo $colegiado->fecha_creacion; ?>
                    </div>
                    <span><?php echo $colegiado->nombre_completo; ?></span>
                    <p><?php echo $colegiado->universidad; ?></p>
                    <br /><br />
                    <p><?php echo $colegiado->especialidad; ?></p>
                </div>
                <?php
            }
        } else
        {
            ?>
            <div class="lista_resultados">
                <div class="fecha_inscrito">
                    Sin resultados
                </div>
            </div>
            <?php
        }
    ?>
</div>