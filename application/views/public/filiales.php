<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    
    </div>
    <div class="txt_titulo">
        filiales
    </div>
</div>
<div id="contenido_nosotros">
  <h1>ENCU&Eacute;NTRANOS AQU&Iacute;</h1>
<span>2017</span><br /><br /><br />
    <div class="col_filiales">
        <div class="tt_col">
        regi&Oacute;n
        </div>
        <div class="cont_col">
            <ul>
                <li>XV Arica y Parinacota</li>
                <li>I Tarapac&aacute;</li>
                <li>II Antofagasta</li>
                <li>III Atacama</li>
                <li>IV Coquimbo</li>
                <li>V Valparaiso</li>
                <li>RM Metropolitana</li>
                <li>VI O`Higgins</li>
                <li>VII Maule</li>
                <li>VIII Biob&iacute;o</li>
                <li>IX Araucan&iacute;a</li>
                <li>XIV Los Rios</li>
                <li>X Los Lagos</li>
                <li>X Los Lagos</li>
                <li>XI Ays&eacute;n</li>
                <li>XII Magallanes</li>
            </ul>
        </div>
    </div>
    <div class="col_filiales">
        <div class="tt_col">
        nombre
        </div>
        <div class="cont_col">
        <ul>
                <li>XV Arica y Parinacota</li>
                <li>I Tarapac&aacute;</li>
                <li>Claudia Rojo</li>
                <li>III Atacama</li>
                <li>IV Coquimbo</li>
                <li>V Valparaiso</li>
                <li>RM Metropolitana</li>
                <li>VI O`Higgins</li>
                <li>VII Maule</li>
                <li>VIII Biob&iacute;o</li>
                <li>IX Araucan&iacute;a</li>
                <li>XIV Los Rios</li>
                <li>X Los Lagos</li>
                <li>X Los Lagos</li>
                <li>XI Ays&eacute;n</li>
                <li>XII Magallanes</li>
            </ul>
        </div>
    </div>
    <div class="col_filiales">
        <div class="tt_col">
        contacto
        </div>
        <div class="cont_col">
        <ul>
                <li><a href="mailto:arica@ciach.cl">arica@ciach.cl</a></li>
                <li><a href="mailto:tarapaca@ciach.cl">tarapaca@ciach.cl</a></li>
                <li><a href="mailto:claudia.rojo@ciach.cl">claudia.rojo@ciach.cl</a></li>
                <li><a href="mailto:atacama@ciach.cl">atacama@ciach.cl</a></li>
                <li><a href="mailto:coquimbo@ciach.cl">coquimbo@ciach.cl</a></li>
                <li><a href="mailto:valparaiso@ciach.cl">valparaiso@ciach.cl</a></li>
                <li><a href="mailto:contacto@ciach.cl">contacto@ciach.cl</a></li>
                <li><a href="mailto:ohiggins@ciach.cl">ohiggins@ciach.cl</a></li>
                <li><a href="mailto:maule@ciach.cl">maule@ciach.cl</a></li>
                <li><a href="mailto:biobio@ciach.cl">biobio@ciach.cl</a></li>
                <li><a href="mailto:araucania@ciach.cl">araucania@ciach.cl</a></li>
                <li><a href="mailto:losrios@ciach.cl">losrios@ciach.cl</a></li>
                <li><a href="mailto:osorno@ciach.cl">osorno@ciach.cl</a></li>
                <li><a href="mailto:puertomontt@ciach.cl">puertomontt@ciach.cl</a></li>
                <li><a href="mailto:aysen@ciach.cl">aysen@ciach.cl</a></li>
                <li><a href="mailto:magallanes@ciach.cl">magallanes@ciach.cl</a></li>
          </ul>
          

        </div>
       
    </div>
     
</div>
<p style="margin-left:20px; font-size:14px; color:#999">Para mayor informaci&oacute;n acerca de filiales escriba a filiales@ciach.cl o contacto@ciach.cl</p>