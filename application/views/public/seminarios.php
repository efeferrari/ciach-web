<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    </div>
    <div class="txt_titulo">
        Seminarios
    </div>
</div>
<div id="contenido_nosotros">
  <h1>REVISA LAS FECHAS DE LOS PR&oacute;XIMOS EVENTOS</h1>
    <span>&nbsp;</span>
</div>
<div id="contenedor_seminarios">
    <?php
        foreach($seminarios as $sem)
        {
            ?>
            <div class="listado_seminarios">
                <span><?php echo $sem->titulo;?></span>
                <p><?php echo $sem->fecha;?></p>
                <img alt="imagen seminario" src="/uploads/img/seminarios/<?php echo $sem->imagen;?>" />
            </div>
            <?php
        }
    ?>
</div>
<div class="enumerador">
    <?php echo $this->pagination->create_links(); ?>
</div>