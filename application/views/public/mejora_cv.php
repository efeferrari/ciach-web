<!-- InstanceBeginEditable name="contenido" -->
<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    </div>
    <div class="txt_titulo">
        mejora tu cv
    </div>
</div>
<div id="contenido_nosotros">
    <h1>revisa las ofertas laborales disponibles</h1>
    <span>encuentra tu oportunidad</span><br /><br /><br />
</div>
<div id="texto_mejora_curriculum">
    <p>
    El concepto de generar nuestro Curriculum Vitae (CV) no es m&aacute;s que el de generar nuestro propio espejo y de c&oacute;mo queremos que las 
    dem&aacute;s personas al leerlo nos piensen, es por lo anterior que esta etapa de actualizaci&oacute;n o de generaci&oacute;n de nuestro CV tenga toda la 
    seriedad y la disposici&oacute;n del tiempo que sea necesario antes de poder masificarlo.<br /><br />

    Es fundamental que antes de comenzar a escribir tu curr&iacute;culum tengas bien clara la informaci&oacute;n que vas a dar, as&iacute; como el orden en 
    que vas a hacerlo. El curr&iacute;culum tiene que presentar los datos de forma clara y concisa, bien explicada y sin rodeos. Ten en cuenta que 
    s&oacute;lo tendr&aacute;s una breve oportunidad de causar buena impresi&oacute;n a quienes te tienen que contratar. Aprov&eacute;chala.<br /><br />

    Por ello, antes de ponerte a redactar el curr&iacute;culum, es importante que te plantees y respondas a una serie de cuestiones.
    Puedes generar un CV general o tipo y este debe ser capaz de adecuarse f&aacute;cilmente a las ofertas en las cuales deseas postular.<br /><br />

    &iexcl;Para qui&eacute;n voy a escribir el curr&iacute;culum? - Es muy importante conocer la empresa para la que quieres trabajar y saber qu&eacute; es lo que 
    busca. As&iacute; podr&aacute;s enfocar el curr&iacute;culum en aquello que puede ofrecerles.<br /><br />

    &iexcl;A qu&eacute; puesto me voy a presentar? - Es bueno tener en mente el puesto concreto y conocer cu&aacute;les son las habilidades que requiere. 
    As&iacute; mismo, conviene tener clara la valoraci&oacute;n de tus experiencia s anteriores para no volver a repetir en funciones en las que no se 
    est&aacute; a gusto.<br /><br />
    &iexcl;Tengo la preparaci&oacute;n necesaria? - Plant&eacute;ate si tienes los requisitos que te exigen. Si los tienes, dest&aacute;calos. Destaca adem&aacute;s si te has 
    reciclado o actualizado recientemente.<br /><br />

    &iexcl;Cu&aacute;les son mis logros profesionales? - C&eacute;ntrate en aquello que sabes hacer bien y de los que te puedes sentir satisfecho. Si tiene que 
    ver con el puesto de trabajo, mucho mejor.<br /><br />

    &iexcl;Cu&aacute;l ha sido mi experiencia profesional anterior? - Analiza tu trayectoria profesional. Busca formas de plasmar positivamente los 
    periodos sin trabajar y encuentra las partes positivas de cada tarea que has hecho.<br /><br />

    &iexcl;Se hablar idiomas? - Los idiomas son uno de las capacidades m&aacute;s demandadas actualmente por las empresas. Si sabes alguno, dest&aacute;calo.
    &iexcl;Domino la inform&aacute;tica? - Haz un listado con aquellos programas que conoces y analiza cu&aacute;l es tu nivel de manejo. Resalta aquellos 
    que puedan interesar al quien ha de contratarte.<br /><br />

    &iexcl;Qu&eacute; otras experiencias puedo acreditar? - Hay muchas experiencias que puede resultar &uacute;til incluirlas en el curr&iacute;culum, todos los cursos 
    dejan algo m&aacute;s sin cursos de especializaci&oacute;n, vale la pena colocar que asisti&oacute; a un seminario u a otro, vale la pena colocar que tengo 
    este curso, esto es sumar, sumar conocimientos: trabajos voluntarios, estancias en el extranjero. Demuestran tu capacidad y tus 
    inquietudes. <br /><br />
        </p>
        <span>Luego del proceso anterior, manos a la obra;</span>
        <p>
        1. Definamos un correo electr&oacute;nico formal y no informal.
    Por ejemplo:<br /> <br />

    Correo informal: juan23_1@jotamail.com, pepita_linda@jotamail.com, pepito_chela@jotamail.com
    Un correo informal en su curr&iacute;culo vitae resta seriedad al mismo.<br /> <br />

    Ejemplos de correos Formales: pedro.perez.p@jotamail.com, juan.perez.p@jotamail.com, paula.perez.p@jotamail.com<br /> <br />

    Como podr&aacute;n observar el tipo de correo formal cambia la visi&oacute;n de inmediato de quien lo env&iacute;a.<br /> <br />

    2. Recomendamos crear un encabezado donde podamos colocar nuestra fotograf&iacute;a a un costado, y nuestros datos principales, nombre 
    completo, Profesi&oacute;n, Direcci&oacute;n, tel&eacute;fonos fijos y celulares, mail. La gracia del encabezado es que se repetir&aacute; autom&aacute;ticamente en cada 
    p&aacute;gina del curriculum vitae.<br />
    3. Luego ya en el cuerpo del CV, colocar un t&iacute;tulo de rese&ntilde;a y objetivos, donde en un p&aacute;rrafo no muy largo y m&aacute;s bien corto seamos 
    capaces de colocar nuestros objetivos laborales y un peque&ntilde;o resumen de nuestras habilidades y experiencias m&aacute;s destacadas.
    4. Luego un nuevo apartado que se dedique para colocar otros antecedentes;<br /> <br />

    Por ejemplo RUT:<br />
    Licencia de conducir:<br />
    Disponibilidad de trasladarse a otra regi&oacute;n:<br />
    Edad:<br />
    Estado Civil:<br />
    Red social: recomendamos www.linkedin.cl (colocar un enlace a su perfil)<br />

    5.- Antecedentes acad&eacute;micos:<br />
    Seguiremos un orden desde lo m&aacute;s nuevo a lo m&aacute;s antiguo.<br /><br />

    Educaci&oacute;n Universitaria: Titulo del estudio, universidad, a&ntilde;o de titulaci&oacute;n, tem&aacute;ticas destacadas, "Titulo de tesis", si con la tesis se 
    gener&oacute; o no alguna publicaci&oacute;n<br />
    Educaci&oacute;n media: nombre del liceo o colegio, "cursos cursados", "a&ntilde;o de egreso"<br />
    Educaci&oacute;n B&aacute;sica: nombre del liceo o colegio, "cursos cursados", "a&ntilde;o de egreso"<br />
    Algunos seguramente han cursado en m&aacute;s de una instituci&oacute;n su educaci&oacute;n media o b&aacute;sica, si gustan colocarlas todas solo se crean el 
    espacio, lo mismo para el caso de otros t&iacute;tulos universitarios, o de liceo t&eacute;cnico, mag&iacute;steres, diplomados solo respetando el orden de lo 
    m&aacute;s nuevo a lo m&aacute;s antiguo".<br />
    </p>
    <span><a href="#">Link de descarga</a></span>
</div>
