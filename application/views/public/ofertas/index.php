<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    
    </div>
    <div class="txt_titulo">
        trabajos y pr&aacute;cticas
    </div>
</div>
<div id="contenido_nosotros">
    <h1>Revisa las ofertas laborales disponibles</h1>
    
    <span>Encuentra tu oportunidad</span>
    
    <?php
        foreach($ofertas as $oferta)
        {
            $link = '/trabajos-practicas/'. $oferta->id . '-'. slugify($oferta->titulo);
            $logo = '/uploads/img/ofertas/logos/' . ((strlen($oferta->logo) > 0)? $oferta->logo : '_default.jpg');
            ?>
            <a href="<?php echo $link; ?>">
                <div class="cont_labor">
                    <div class="img_labor">
                        <img src="<?php echo $logo; ?>" alt='<?php echo $oferta->titulo; ?>' />
                    </div>
                    <div class="txt_labor">
                        <div class="titulo_prev_oferta">
                            <?php echo $oferta->titulo; ?>
                        </div>
                        <div class="texto_prev_oferta">
                            <?php echo plain_text($oferta->descripcion); ?>
                        </div>
                    </div>
                    <div class="fecha_labor">
                        <?php echo reformat_date($oferta->fecha_creacion);?>
                    </div>
                </div>
            </a>
            <?php
        }
    ?>
    
</div>

<div class="enumerador">
    <?php echo $this->pagination->create_links(); ?>
</div>

<!--
<br>
<div class="enumerador">
    <a href="#">&lt;&lt;Primero</a>
    <a href="#">&lt;Anterior</a>
    1
    <a href="#">2</a>
    <a href="#">3</a>
    <a href="#">4</a>
    <a href="#">Siguiente &gt;</a>
    <a href="#">&Uacuteltimo &gt;&gt;</a>
</div>
-->
