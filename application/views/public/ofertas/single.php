<div class="titulo">
    <div class="logo_pequeno">
        <img src="/assets/site/img/logo_chico_contenidos_titulos.png"/>
    
    </div>
    <div class="txt_titulo">
        Trabajos y pr&aacute;cticas
    </div>
</div>
<div id="contenido_nosotros">
    <h1>Revisa las ofertas laborales disponibles</h1>
    <span>encuentra tu oportunidad</span>
</div>
<div class="contenido_oferta">
    <div class="superior_oferta">
        <div id="superior_oferta_imagen">
            <img src="<?php echo '/uploads/img/ofertas/logos/' . ((strlen($oferta->logo) > 0)? $oferta->logo : '_default.jpg');?>" />
        </div>
        <div id="superior_oferta_titulo">
            <p><?php echo $oferta->titulo; ?><p>
        </div>
        <div id="superior_oferta_fecha">
            <p><?php echo reformat_date($oferta->fecha_creacion, 'd-m-Y H:i'); ?></p>
        
        </div>
    </div>
    <div id="texto_oferta">
        <?php echo $oferta->descripcion; ?>
    </div>
</div>