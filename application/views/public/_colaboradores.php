<?php
    
    $ci =& get_instance();

    $ci->load->model('link_model');
    
    $params = array(
        'tipo'   => 1,
        'estado' => 1,
    );
    
    $colaboradores = $ci->link_model->obtener_por_parametros($params, 300);
    
    if( count( $colaboradores ) > 0 )
    {
        ?><div id="index_colaboradores">
            <img src="/assets/site/img/logo_chico_contenidos.png" /><section>colaboradores</section>
            <div id="carru_colaboradores" class='owl-carousel'>
            <?php
                foreach( $colaboradores as $c )
                {
                    ?>
                        <div><img src="/uploads/img/links/<?php echo $c->imagen;?>" /></div>
                    <?php
                }
            ?>
            </div>
        </div>
        <?php
    }
?>
        
    