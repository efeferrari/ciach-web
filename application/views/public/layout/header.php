<?php

$user_session = $this->session->userdata('ciach');

// echo "<pre>"; print_r( $user_session ); echo "</pre>"; die;

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/base 1.dwt" codeOutsideHTMLIsLocked="false" -->
<head lang="es">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Colegio de Ingenieros en alimentos de chile</title>
    
    <link rel="icon" href="/assets/favicon.ico" type="image/ico"/>
    <link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon"/>

    
    <link href="/assets/site/css/estilo.css" rel="stylesheet" type="text/css" />
    <link href="/assets/site/libs/owl/assets/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/site/libs/owl/assets/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    
    <link href="/assets/site/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    
</head>
<body>
<header>
	<div id="cont_header">
		<div id="logo">
        	<a href="/"><img src="/assets/site/img/logo.png" /></a>
    	</div>
    	<div id="botones_arriba">
        	<div id="botonera_arriba">
            	<ul>
                	<a href="/"><li class="boton_ppal">Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li></a>
                    <li class="espaciador_btn"></li>
                    <a href="/nosotros"><li class="boton_ppal">Nosotros&nbsp;&nbsp;&nbsp;&nbsp;</li></a>
                    <li class="espaciador_btn"></li>
                    <a href="/contacto"><li class="boton_ppal">Contacto&nbsp;&nbsp;&nbsp;&nbsp;</li></a>
                    <li class="espaciador_btn"></li>
                    
                    <?php
                        if(!empty($user_session) && isset($user_session['tipo']) && $user_session['tipo'] == 1)
                        {
                            ?><a href="/admin"><li class="boton_ppal">Administrar&nbsp;&nbsp;&nbsp;</li></a><?php
                        }
                    ?>
				</ul>
            </div>
    		<div id="ident_sesion">
                <?php
                
                    if(empty($user_session) || !isset($user_session['nombre']))
                    {
                        ?>
                            Hola Invitado&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/entrar">Iniciar sesi&oacute;n</a>
                        <?php
                    } else 
                    {
                        ?>
                            Hola, <?php echo $user_session['nombre'] ?> | <a href="/salir">Salir</a>
                        <?php
                    }
                
                ?>
       		</div>
        </div>
    </div>
    <div id="contenedor_carrusel">
    	<div id="carrusel">
    	<img src="/assets/site/img/carru_temp.jpg" />
		</div>
    </div>
</header>

<!-- start: central -->
<div id="central">
	<div id="carrusel">
    	<img src="/assets/site/img/carru_temp.jpg" />
	</div>
    <div id="botones">
        <ul id="botonera">
            <li class="boton_menu"><a href="/trabajos-practicas">Trabajo y pr&aacute;cticas</a>
                <ul>
                    <li class="boton_desp"><a href="/mejora-tu-cv">Mejora tu CV</a></li>
                </ul>
            </li>
            <li class="boton_menu"><a href="/seminarios">Seminarios</a></li>
            <li class="boton_menu"><a href="/el-insigne">El Insigne</a></li>
            <li class="boton_menu"><a href="/filiales">Filiales CIACh</a>
                <ul>
                    <li class="boton_desp"><a href="/universidades">Universidades</a></li>
                </ul>
            </li>
            <li class="boton_menu"><a href="/colegiarse">Colegiarse</a></li>
            <li class="boton_menu"><a href="/estatuto-codigo-etica">Estatutos y &eacute;tica</a></li>
            <li class="boton_menu"><a href="/colegiados">Buscador colegiados</a></li>
        </ul>
    </div>
    
    <!-- start: contenido -->
    <div id="contenido">
        