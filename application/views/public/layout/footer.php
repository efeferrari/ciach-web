        
    </div><!-- end: contenido -->
</div><!-- end: central -->

<footer>
	<div id="cont_footer">
        <div id="rrss"><br /><br /> 
            S&Iacute;GUENOS:<br /><br />
            <a class="boton_rrss" href="https://www.facebook.com/"><img src="/assets/site/img/_0000_fb.png" /></a>
            <a class="boton_rrss" href="https://twitter.com/"><img src="/assets/site/img/_0001_tw.png" /></a>
            <a class="boton_rrss" href="https://cl.linkedin.com/"><img src="/assets/site/img/_0002_in.png" /></a>
            <a class="boton_rrss" href="https://plus.google.com/"><img src="/assets/site/img/_0003_goo.png" /></a>
            <br /><br />
            Contacto: info@ciach.cl
        </div>
        <div id="links">
            <ul>
                <li><a href="/nosotros">Nosotros</a></li>
                <li><a href="/colegiarse">Colegiarse</a></li>
                <li><a href="/filiales">Filiales CIACh</a></li>
                <li><a href="/el-insigne">El Insigne</a></li>
                <li><a href="/estatuto-codigo-etica">Estatuto y c&oacute;digo </br> de &eacute;tica</a></li>
            </ul>
            <ul>
                <li><a href="/trabajos-practicas">Trabajos y pr&aacute;cticas</a></li>
                <li><a href="/mejora-tu-cv">Mejora tu CV</a></li>
                <li><a href="/seminarios">Seminarios</a></li>
                <li><a href="/universidades">Universidades</a></li>
            </ul>
            <ul>
                <li><a href="/links-de-interes">Link de inter&eacute;s</a></li>
                <li><a href="/">Noticias</a></li>
                <li><a href="/colaboradores">Colaboradores</a></li>
            </ul>
        </div>
        <div id="ddaa">
           Todos los derechos reservados. Colegio de Ingenieros en Alimentos, <?php echo date('Y'); ?>.
        </div>
	</div>
</footer>
<script src="/assets/site/libs/jquery-3.2.1.min.js"></script>
<script src="/assets/site/libs/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/site/libs/owl/owl.carousel.min.js"></script>

<script src="/assets/site/js/_functions.js"></script>
<?php
    if($this->router->class == 'colegiarse')
    {
        ?><script src="/assets/site/js/colegiarse.js"></script><?php
    }
    
    if($this->router->class == 'colegiados')
    {
        ?><script src="/assets/site/js/colegiados.js"></script><?php
    }
?>
<script>
    $(function()
    {
        if( $('#index_colaboradores').length > 0 )
        {
            var params =
            {
                items:4,
                loop:true,
                margin:10,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true
            };
            
            $("#carru_colaboradores").owlCarousel(params);
        }
    });
</script>
</body>
</html>
