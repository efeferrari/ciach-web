<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-newspaper-o"></i>Detalle
        
        <div class="panel-tools">
            
            
                <?php
                
                    if($seminario->estado == 0)
                    {
                        ?>
                        <button type="button" class="btn btn-gray btn-xs">
                            Borrador
                        </button>
                        <?php
                    }
                ?>
            
            <a href="/admin/seminarios/editar/<?php echo $seminario->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-blue btn-xs">
                    Editar
                </button></a>
            
            <a href="/admin/seminarios/eliminar/<?php echo $seminario->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-red btn-xs">
                    Eliminar
                </button></a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="2"><?php echo $seminario->titulo; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td nowrap>
                        Creado por
                    </td>
                    <td>
                        <?php
                        echo $seminario->autor;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td nowrap>
                        Fecha
                    </td>
                    <td>
                        <?php
                        echo $seminario->fecha;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Imagen
                    </td>
                    <td>
                        <?php
                            if(strlen($seminario->imagen) > 0 )
                            {
                                ?><img src='<?php echo '/uploads/img/links/' . $seminario->imagen; ?>' alt='Imagen no disponible' style='max-width:30%;'><?php
                            } else
                            {
                                echo 'No se adjunt&oacute; imagen';
                            }
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>