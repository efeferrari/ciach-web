<?php

echo form_open_multipart('', array('class' => 'form-horizontal', 'id' => 'seminario_form'));

?>
<div class="form-group">
    <label class="col-sm-2 control-label" for="seminario_titulo">T&iacute;tulo</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'seminario[titulo]', 'id' => 'seminario_nombre', 'class' => 'form-control', 'value' => isset($seminario)? $seminario->titulo : '', 'aria-required' => 'true')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="seminario_fecha">Fecha</label>
    <div class="col-sm-9">
        <div class="input-group" id='picker-container'>
            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
            <?php echo form_input(array('name' => 'seminario[fecha]', 'id' => 'seminario_nombre', 'class' => 'form-control date-picker', 'value' => isset($seminario)? $seminario->fecha : '', 'aria-required' => 'true', 'data-date-format' => 'dd-mm-yyyy', 'data-date-viewmode' => 'years')); ?>
        </div>
    </div>
</div>

<div class="form-group" id='img_container'>
    <label class="col-sm-2 control-label" for="seminario_imagen">Imagen</label>
    <div class="col-sm-9">
        <?php echo form_upload(array('name' => 'imagen', 'id' => 'seminario_imagen', 'class' => 'file', 'data-show-preview' => 'false')); ?>
        
        <?php
            if($this->router->fetch_method() != 'crear' && isset($seminario))
            {
                if(strlen($seminario->imagen) > 0)
                {
                    ?>
                        <a data-toggle="modal" class="btn btn-info btn-xs" role="button" href="#modal_imagen_actual">Ver imagen actual</a>
                        
                        <div class="modal fade" id="modal_imagen_actual" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title"><?php echo $seminario->imagen; ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <img style='width:100%;' src='/uploads/img/seminarios/<?php echo $seminario->imagen; ?>'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <label class="checkbox-inline">
                            <input name="seminario[_eliminar_imagen]" type="checkbox" class="red" value="<?php echo $seminario->imagen; ?>">
                            &iquest;Eliminar imagen?
                        </label>
                    <?php
                }
            }
        ?>
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-9">
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'seminario[estado]', 'id' => 'seminario_estado_on', 'value' => 1, 'checked' => (isset($seminario) && $seminario->estado == 1)? true : false, 'class' => 'square-green')); ?>
            Publicada
        </label>
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'seminario[estado]', 'id' => 'seminario_estado_off', 'value' => 0, 'checked' => (isset($seminario) && $seminario->estado == 0)? true : (!isset($seminario)? true : false), 'class' => 'square-red')); ?>
            Borrador
        </label>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-9">
        <a class="btn btn-primary" href="/admin/seminarios">Cancelar</a>
        <?php
            echo form_submit(array('class' => 'btn btn-green', 'value' => 'Guardar'));
        ?>
    </div>
</div>

<div class='hidden_data'>
    <?php
    
    if($this->router->fetch_method() != 'crear')
    {
        echo form_hidden(array('seminario[id]' => isset($seminario)? $seminario->id : ''));
    } else
    {
        echo form_hidden(array('seminario[autor]' => $this->session->userdata('ciach')['id'] ));
    }
    
    ?>
</div>
<?php

echo form_close();



