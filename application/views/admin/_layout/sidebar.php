<div class="navbar-content">
    <!-- start: SIDEBAR -->
    <div class="main-navigation navbar-collapse collapse">
        <!-- start: MAIN MENU TOGGLER BUTTON -->
        <div class="navigation-toggler">
            <i class="clip-chevron-left"></i>
            <i class="clip-chevron-right"></i>
        </div>
        <!-- end: MAIN MENU TOGGLER BUTTON -->
        <!-- start: MAIN NAVIGATION MENU -->
        <ul class="main-navigation-menu">
            <li>
                <a href="/">
                    <i class="fa clip-paperplane"></i>
                    <span class="title"> Sitio </span>
                </a>
            </li>
            <li>
                <a href="/admin/noticias/">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title"> Noticias </span>
                </a>
            </li>
            <li>
                <a href="/admin/bolsa-de-trabajo">
                    <i class="fa fa-briefcase"></i>
                    <span class="title"> Bolsa de trabajo </span>
                </a>
            </li>
            <li>
                <a href="/admin/links">
                    <i class="clip-link"></i>
                    <span class="title"> Links </span></i>
                    <span class="selected"></span>
                </a>
            </li>
            <li>
                <a href="/admin/seminarios">
                    <i class="fa fa-group"></i>
                    <span class="title"> Seminarios </span></i>
                    <span class="selected"></span>
                </a>
            </li>
            <li>
                <a href="/admin/usuarios">
                    <i class="clip-users"></i>
                    <span class="title"> Usuarios</span>
                </a>
            </li>
        </ul>
        <!-- end: MAIN NAVIGATION MENU -->
    </div>
    <!-- end: SIDEBAR -->
</div>