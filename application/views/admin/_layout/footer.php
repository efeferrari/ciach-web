
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            &copy; Developed by XCF <?php echo date('Y');?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="/assets/admin/bower_components/respond/dest/respond.min.js"></script>
        <script src="/assets/admin/bower_components/Flot/excanvas.min.js"></script>
        <script src="/assets/admin/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="/assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="/assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="/assets/admin/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <?php
        $__controlador = $this->router->fetch_class();
        $__accion      = $this->router->fetch_method();
        
        if($__controlador == 'noticias' || $__controlador == 'links')
        {
            if($__accion == 'crear' || $__accion == 'editar')
            {
            ?>
                <script src="/assets/admin/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
                <script src="/assets/admin/bower_components/ckeditor/ckeditor.js"></script>
                <script src="/assets/admin/bower_components/ckeditor/adapters/jquery.js"></script>
                <script src="/assets/admin/bower_components/summernote/dist/summernote.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
                <script src="/assets/admin/js/ciach/form-validations.js"></script>
                <?php
            }
        }
        
        if($__controlador == 'ofertas')
        {
            if($__accion == 'crear' || $__accion == 'editar')
            {
            ?>
                <script src="/assets/admin/bower_components/ckeditor/ckeditor.js"></script>
                <script src="/assets/admin/js/ciach/form-validations.js"></script>
                <?php
            }
        }
        
        if($__controlador == 'seminarios')
        {
            if($__accion == 'crear' || $__accion == 'editar')
            {
                ?>
                <script src="/assets/admin/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
                <script src="/assets/admin/bower_components/ckeditor/ckeditor.js"></script>
                <script src="/assets/admin/bower_components/ckeditor/adapters/jquery.js"></script>
                <script src="/assets/admin/bower_components/summernote/dist/summernote.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
                
                <!--
                <script src="/assets/admin/bower_components/moment/min/moment.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
                <script src="/assets/admin/bower_components/autosize/dist/autosize.min.js"></script>
                <script src="/assets/admin/bower_components/select2/dist/js/select2.min.js"></script>
                <script src="/assets/admin/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
                <script src="/assets/admin/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
                <script src="/assets/admin/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
                <script src="/assets/admin/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
                <script src="/assets/admin/bower_components/summernote/dist/summernote.min.js"></script>
                <script src="/assets/admin/bower_components/ckeditor/ckeditor.js"></script>
                <script src="/assets/admin/bower_components/ckeditor/adapters/jquery.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
                <script src="/assets/admin/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
                -->
                <script src="/assets/admin/js/ciach/form-validations.js"></script>
                <?php
            }
        }
    ?>
    
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script>
        jQuery(document).ready(function()
        {
            Main.init();
            
            if(typeof FormValidator != 'undefined')
                FormValidator.init();
            
            <?php
                // Mostrar Mensajes flash
                $alertas = $this->session->flashdata('alertas');
                
                if(is_array($alertas) && count($alertas) > 0)
                {
                    $encabezados = array
                    (
                        'success' => '&Eacute;xito',
                        'error'   => 'Error',
                        'warning' => 'Aviso',
                    );
                    
                    foreach($alertas as $tipo => $mensaje)
                    {
                        ?>
                        $.toast({
                            heading: '<?php echo $encabezados[$tipo]; ?>',
                            text: '<?php echo $mensaje?>',
                            showHideTransition: 'slide',
                            position: 'top-right',
                            icon: '<?php echo $tipo; ?>',
                            loader: false,
                            timeOut: 5
                        });
                        <?php
                    }
                }
            ?>
        });
    </script>
</body>

</html>