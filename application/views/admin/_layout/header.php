<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>CIACH :: Administraci&oacute;n</title>
    <link rel="icon" href="/assets/favicon.ico" type="image/ico"/>
    <link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon"/>
    
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" media="print" href="/assets/admin/css/print.min.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/bootstrap-fileinput/css/fileinput.min.css" />
    
    <link type="text/css" rel="stylesheet" id="skin_color" href="/assets/admin/css/theme/light.min.css" />
    
    <!--
    <link type="text/css" rel="stylesheet" id="skin_color" href="/assets/admin/css/theme/green.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="/assets/admin/css/theme/navy.min.css" />
    -->
    
    <!-- START: CSS especifics -->
    <?php
        $__controlador = $this->router->fetch_class();
        $__accion      = $this->router->fetch_method();
        
        if($__controlador == 'noticias' || $__controlador == 'links' || $__controlador == 'seminarios')
        {
            if($__accion == 'crear' || $__accion == 'editar')
            {
                ?>
                    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/bootstrap-fileinput/css/fileinput.min.css" />
                    <link type="text/css" rel="stylesheet" href="/assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
                    <link type="text/css" rel="stylesheet" href="/assets/admin/plugin/bootstrap-timepicker.min.css" />
                <?php
            }
        }
    
    ?>
    <!-- END: CSS especifics -->
    
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

</head>

<body class='footer-fixed'>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- start: LOGO -->
                <a class="navbar-brand" href="index.html">
                    CLIP<i class="clip-clip"></i>ONE
                </a>
                <!-- end: LOGO -->
            </div>
            <div class="navbar-tools">
                <!-- start: TOP NAVIGATION MENU -->
                <ul class="nav navbar-right">
                    <!-- start: USER DROPDOWN -->
                    <li class="dropdown current-user">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                            <?php
                            
                                $tmp = $this->session->userdata('ciach');
                            
                            
                            ?>
                            <span class="username"><?php echo $tmp['nombre'] ?></span>
                            <i class="clip-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/admin/usuarios/ver/<?php echo $tmp['id']; ?>">
                                    <i class="clip-user-2"></i> &nbsp;Mi perfil
                                </a>
                            </li>
                            <li>
                                <li class="divider"></li>
                                <li>
                                    <a href="/salir">
                                        <i class="clip-exit"></i> &nbsp;Salir
                                    </a>
                                </li>
                            </li>
                        </ul>
                    </li>
                    <!-- end: USER DROPDOWN -->
                </ul>
                <!-- end: TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
    
        <!-- START: load sidebar menu -->
            <?php $this->load->view('admin/_layout/sidebar'); ?>
        <!-- END: load sidebar menu -->

        <!-- start: PAGE -->
        <div class="main-content">
            <!-- start: PANEL CONFIGURATION MODAL FORM -->
            <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                            <h4 class="modal-title">Panel Configuration</h4>
                        </div>
                        <div class="modal-body">
                            Here will be a configuration form
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                            <button type="button" class="btn btn-primary">
                    Save changes
                </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- end: SPANEL CONFIGURATION MODAL FORM -->
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-cog-2"></i>
                                <a href="/admin/">
                                    Administraci&oacute;n
                                </a>
                            </li>
                            <li class="active">
                                <?php echo $breadcrum_titulo; ?>
                            </li>
                        </ol>
                        <div class="page-header">
                            <h1><?php echo $breadcrum_titulo; ?> <small><?php echo $breadcrum_subtitulo; ?></small></h1>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                    
                </div>
                <!-- end: PAGE HEADER -->
                
                <!-- start: PAGE CONTENT -->
                