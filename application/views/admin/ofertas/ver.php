<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-newspaper-o"></i>Detalle
        
        <div class="panel-tools">
            <?php
                if($oferta->estado == 0)
                {
                    ?>
                    <button type="button" class="btn btn-gray btn-xs">
                        Borrador
                    </button>
                    <?php
                }
            ?>
            <a href="/admin/ofertas/editar/<?php echo $oferta->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-blue btn-xs">
                    Editar
                </button></a>
            
            <a href="/admin/ofertas/eliminar/<?php echo $oferta->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-red btn-xs">
                    Eliminar
                </button></a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="2"><?php echo $oferta->titulo; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php
                        if(strlen($oferta->logo) > 0)
                        {
                            ?>
                            <img style='max-height:200px;' src="/uploads/img/ofertas/logos/<?php echo $oferta->logo; ?>">
                            <?php
                        } else
                        {
                            ?>Sin logo asociado<?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        echo $oferta->descripcion;
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>