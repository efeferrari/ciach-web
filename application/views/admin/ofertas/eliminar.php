
<div class="row">
    <div class="col-sm-12">
        <div class="core-box">
            <div class="heading">
                <i class="fa fa-trash circle-icon circle-bricky"></i>
                <h2>&iquest;Confirma que desea eliminar este enlace? La acci&oacute;n es irreversible</h2>
            </div>
            <form action="" method="post">
                <input type="submit" name="eliminar" value="Eliminar" class="view-more btn btn-bricky"></input>
            </form>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-newspaper-o"></i> Detalles
    </div>
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="2"><?php echo $oferta->titulo; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php
                        echo $oferta->descripcion;
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
