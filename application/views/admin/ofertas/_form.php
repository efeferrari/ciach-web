<?php

echo form_open_multipart('', array('class' => 'form-horizontal', 'id' => 'oferta_form'));

?>
<div class="form-group">
    <label class="col-sm-2 control-label" for="oferta_titulo">T&iacute;tulo</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'oferta[titulo]', 'id' => 'oferta_nombre', 'class' => 'form-control', 'value' => isset($oferta)? $oferta->titulo : '', 'aria-required' => 'true')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="oferta_descripcion">Descripci&oacute;n</label>
    <div class="col-sm-9">
        <?php echo form_textarea(array('name' => 'oferta[descripcion]', 'id' => 'oferta_descripcion', 'class' => 'ckeditor form-control', 'cols' => 10, 'rows' => 10, 'value' => isset($oferta)? $oferta->descripcion : '')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="noticia_imagen">Logo empresa</label>
    <div class="col-sm-9">
    
        <?php echo form_upload(array('name' => 'logo', 'id' => 'oferta_imagen', 'class' => 'file', 'data-show-preview' => 'false')); ?>
        
        <?php
            if($this->router->fetch_method() != 'crear' && isset($oferta))
            {
                if(strlen($oferta->logo) > 0)
                {
                    ?>
                        <a data-toggle="modal" class="btn btn-info btn-xs" role="button" href="#modal_imagen_actual">Ver imagen actual</a>
                        
                        <div class="modal fade" id="modal_imagen_actual" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title"><?php echo $oferta->logo; ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <img style='width:100%;' src='/uploads/img/ofertas/logos/<?php echo $oferta->logo; ?>'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <label class="checkbox-inline">
                            <input name="oferta[_eliminar_logo]" type="checkbox" class="red" value="<?php echo $oferta->logo; ?>">
                            &iquest;Eliminar imagen?
                        </label>
                    <?php
                }
            }
        ?>
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-9">
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'oferta[estado]', 'id' => 'oferta_estado_on', 'value' => 1, 'checked' => (isset($oferta) && $oferta->estado == 1)? true : false, 'class' => 'square-green')); ?>
            Publicada
        </label>
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'oferta[estado]', 'id' => 'oferta_estado_off', 'value' => 0, 'checked' => (isset($oferta) && $oferta->estado == 0)? true : (!isset($oferta)? true : false), 'class' => 'square-red')); ?>
            Borrador
        </label>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-9">
        <a class="btn btn-primary" href="/admin/ofertas">Cancelar</a>
        <?php
            echo form_submit(array('class' => 'btn btn-green', 'value' => 'Guardar'));
        ?>
    </div>
</div>

<div class='hidden_data'>
    <?php
    
    if($this->router->fetch_method() != 'crear')
    {
        echo form_hidden(array('oferta[id]' => isset($oferta)? $oferta->id : ''));
    } else
    {
        echo form_hidden(array('oferta[autor]' => $this->session->userdata('ciach')['id'] ));
    }
    
    ?>
</div>
<?php

echo form_close();



