<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-newspaper-o"></i>Detalle
        
        <div class="panel-tools">
            
            
                <?php
                
                    if($noticia->estado == 1)
                    {
                        ?>
                        <a href="/noticia/<?php echo $noticia->id; ?>-<?php echo slugify($noticia->titulo); ?>" data-placement="top" data-original-title="Editar">
                            <button type="button" class="btn btn-green btn-xs">
                                Ver Publicado
                            </button></a>
                        <?php
                    } else
                    {
                        ?>
                        <button type="button" class="btn btn-gray btn-xs">
                            Borrador
                        </button>
                        <?php
                    }
                ?>
            
            <a href="/admin/noticias/editar/<?php echo $noticia->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-blue btn-xs">
                    Editar
                </button></a>
            
            <a href="/admin/noticias/eliminar/<?php echo $noticia->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-red btn-xs">
                    Eliminar
                </button></a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="2"><?php echo $noticia->titulo; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Escrita por</td>
                    <td>
                        Imagen destacada
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $noticia->u_nombre; ?>
                    </td>
                    <td>
                        <?php
                        if(strlen($noticia->imagen) > 0)
                        {
                            ?>
                            <img style='max-height:200px;' src="/uploads/img/noticias/destacadas/<?php echo $noticia->imagen; ?>">
                            <?php
                        } else
                        {
                            ?>Sin imagen definida<?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
            </tbody>
        </table>
        <?php echo $noticia->contenido;?>
    </div>
</div>