<?php

echo form_open_multipart('', array('class' => 'form-horizontal', 'id' => 'noticia_form'));

?>
<div class="form-group">
    <label class="col-sm-2 control-label" for="noticia_titulo">T&iacute;tulo</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'noticia[titulo]', 'id' => 'noticia_titulo', 'class' => 'form-control', 'value' => isset($noticia)? $noticia->titulo : '', 'aria-required' => 'true')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="noticia_subtitulo">Sub T&iacute;tulo</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'noticia[subtitulo]', 'id' => 'noticia_subtitulo', 'class' => 'form-control', 'value' => isset($noticia)? $noticia->subtitulo : '', 'aria-required' => 'true')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="noticia_imagen">Imagen destacada</label>
    <div class="col-sm-9">
        <?php echo form_upload(array('name' => 'imagen', 'id' => 'noticia_imagen', 'class' => 'file', 'data-show-preview' => 'false')); ?>
        
        <?php
            if($this->router->fetch_method() != 'crear' && isset($noticia))
            {
                if(strlen($noticia->imagen) > 0)
                {
                    ?>
                        <a data-toggle="modal" class="btn btn-info btn-xs" role="button" href="#modal_imagen_actual">Ver imagen actual</a>
                        
                        <div class="modal fade" id="modal_imagen_actual" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title"><?php echo $noticia->imagen; ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <img style='width:100%;' src='/uploads/img/noticias/destacadas/<?php echo $noticia->imagen; ?>'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <label class="checkbox-inline">
                            <input name="noticia[_eliminar_imagen]" type="checkbox" class="red" value="<?php echo $noticia->imagen; ?>">
                            &iquest;Eliminar imagen?
                        </label>
                    <?php
                }
            }
        ?>
        
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label" for="noticia_contenido">Contenido</label>
    <div class="col-sm-9">
        <?php echo form_textarea(array('name' => 'noticia[contenido]', 'id' => 'noticia_contenido', 'class' => 'ckeditor form-control', 'cols' => 10, 'rows' => 10, 'value' => isset($noticia)? $noticia->contenido : '')); ?>
    </div>
</div>

<div class="form-group">

    <label class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-9">
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'noticia[estado]', 'id' => 'noticia_estado_on', 'value' => 1, 'checked' => (isset($noticia) && $noticia->estado == 1)? true : false, 'class' => 'square-green')); ?>
            Publicada
        </label>
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'noticia[estado]', 'id' => 'noticia_estado_off', 'value' => 0, 'checked' => (isset($noticia) && $noticia->estado == 0)? true : (!isset($noticia)? true : false), 'class' => 'square-red')); ?>
            Borrador
        </label>
    </div>
    
</div>

<div class="form-group">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-9">
        <a class="btn btn-primary" href="/admin/noticias">Cancelar</a>
        <?php
            echo form_submit(array('class' => 'btn btn-green', 'value' => 'Guardar'));
        ?>
    </div>
</div>

<div class='hidden_data'>
    <?php
    
    if($this->router->fetch_method() != 'crear')
    {
        echo form_hidden(array('noticia[id]' => isset($noticia)? $noticia->id : ''));
    } else
    {
        echo form_hidden(array('noticia[autor]' => $this->session->userdata('ciach')['id'] ));
    }
    
    ?>
</div>
<?php

echo form_close();



