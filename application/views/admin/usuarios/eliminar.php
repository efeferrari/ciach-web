
<div class="row">
    <div class="col-sm-12">
        <div class="core-box">
            <div class="heading">
                <i class="fa fa-trash circle-icon circle-bricky"></i>
                <h2>&iquest;Confirma que desea eliminar a <?php echo $usuario->nombre; ?>? La acci&oacute;n es irreversible</h2>
            </div>
            <form action="" method="post">
                <input type="submit" name="eliminar" value="Eliminar" class="view-more btn btn-bricky"></input>
            </form>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="clip clip-user"></i> Detalles
    </div>
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="3"><?php echo $usuario->nombre; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Correo</td>
                    <td>
                        <a href="mailto:<?php echo $usuario->correo; ?>">
                            <?php echo $usuario->correo; ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Estado</td>
                    <td>
                        <?php
                        
                        if($usuario->estado == 1)
                        {
                            ?>
                                <span class="label label-success">Activo</span>
                            <?php
                        } else
                        {
                            ?>
                                <span class="label label-warning">Inactivo</span>
                            <?php
                        }
                        
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Fecha de registro</td>
                    <td>
                        <?php echo reformat_date($usuario->fecha_creacion, 'd-m-Y \a \l\a\s H:i:s'); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
