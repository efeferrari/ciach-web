<?php

echo form_open('', array('class' => 'form-horizontal', 'id' => 'usuario_form'));

?>
<div class="form-group">
    <label class="col-sm-2 control-label" for="usuario_nombre">Nombre Completo</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'usuario[nombre]', 'id' => 'usuario_nombre', 'class' => 'form-control', 'value' => isset($usuario)? $usuario->nombre : '', 'aria-required' => 'true')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="usuario_correo">Correo</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'usuario[correo]', 'id' => 'usuario_correo', 'class' => 'form-control', 'value' => isset($usuario)? $usuario->correo : '')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="usuario_clave">Clave</label>
    <div class="col-sm-9">
        <?php
            echo form_password(array('name' => 'usuario[clave]', 'id' => 'usuario_clave', 'class' => 'form-control', 'value' => ''));
            
            if($this->router->fetch_method() == 'editar')
            {
                ?>
                <span class="help-block"><i class="fa fa-info-circle"></i> Ingrese la clave s&oacute;lo si desea modificarla, en caso contrario mantenga el campo en blanco</span>
                <?php
            }
            
        ?>
    </div>
</div>



<div class="form-group">
    <label class="col-sm-2 control-label" for="usuario_tipo">Tipo</label>
    <div class="col-sm-9">
        <?php
            $selected = isset($usuario)? $usuario->tipo_id : null;
            echo form_dropdown('usuario[tipo]', $this->tipos_usuario, $selected, 'id="usuario_tipo" class="form-control"');
        ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="usuario_tipo">Estado</label>
    <div class="col-sm-9">
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'usuario[estado]', 'id' => 'usuario_estado_on', 'value' => 1, 'checked' => (isset($usuario) && $usuario->estado == 1)? true : false, 'class' => 'square-green')); ?>
            Activo
        </label>
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'usuario[estado]', 'id' => 'usuario_estado_off', 'value' => 0, 'checked' => (isset($usuario) && $usuario->estado == 0)? true : (!isset($usuario)? true : false), 'class' => 'square-red')); ?>
            Inactivo
        </label>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-9">
        <a class="btn btn-primary" href="/admin/usuarios">Cancelar</a>
        
        <?php
            echo form_submit(array('class' => 'btn btn-green', 'value' => 'Guardar'));
        ?>
    </div>
</div>

<?php

if($this->router->fetch_method() != 'crear')
{
    echo form_hidden(array('usuario[id]' => isset($usuario)? $usuario->id : ''));
}

echo form_close();



