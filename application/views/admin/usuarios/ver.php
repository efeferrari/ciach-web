<div class="panel panel-default">
    <div class="panel-heading">
        <i class="clip clip-user"></i> Detalles
        
        <div class="panel-tools">
            
            <a href="/admin/usuarios/editar/<?php echo $usuario->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-blue btn-xs">
                    Editar
                </button></a>
            
            <a href="/admin/usuarios/eliminar/<?php echo $usuario->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-red btn-xs">
                    Eliminar
                </button></a>
            
            
        
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="3"><?php echo $usuario->nombre; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Correo</td>
                    <td>
                        <a href="mailto:<?php echo $usuario->correo; ?>">
                            <?php echo $usuario->correo; ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Estado</td>
                    <td>
                        <?php
                        
                        if($usuario->estado == 1)
                        {
                            ?>
                                <span class="label label-success">Activo</span>
                            <?php
                        } else
                        {
                            ?>
                                <span class="label label-warning">Inactivo</span>
                            <?php
                        }
                        
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Fecha de registro</td>
                    <td>
                        <?php echo reformat_date($usuario->fecha_creacion, 'd-m-Y \a \l\a\s H:i:s'); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>