
<div class='btn_container'>
    <a class="btn btn-green" href="/admin/usuarios/crear">
        <i class="fa fa-plus"></i> Nuevo usuario
    </a>
</div>

<br><!-- Lindo parshe oe -->

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">#</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th class="hidden-xs">Tipo</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    
        <?php
            foreach($usuarios as $n => $usuario)
            {
                $url_view   = $this->config->item('base_url') . 'admin/usuarios/ver/' . $usuario->id;
                $url_edit   = $this->config->item('base_url') . 'admin/usuarios/editar/' . $usuario->id;
                $url_delete = $this->config->item('base_url') . 'admin/usuarios/eliminar/' . $usuario->id;
                
                ?>
                <tr>
                    <td class="center"><?php echo $n+1; ?></td>
                    <td><?php echo $usuario->nombre; ?></td>
                    <td><?php echo $usuario->correo; ?></td>
                    <td class="hidden-xs"><?php echo $usuario->tipo; ?></td>
                    
                    <td><?php echo ($usuario->estado == 1)? '<span class="label label-success"> Activo</span>' : '<span class="label label-warning"> Inactivo</span>'; ?></td>
                    <td class="center">
                    
                        <div class="visible-md visible-lg hidden-sm hidden-xs">
                            <a href="<?php echo $url_view; ?>"   class="btn btn-green tooltips" data-placement="top" data-original-title="Ver"><i class="clip clip-eye"></i></a>
                            <a href="<?php echo $url_edit; ?>"   class="btn btn-blue tooltips" data-placement="top" data-original-title="Editar"><i class="clip clip-pencil-3"></i></a>
                            <a href="<?php echo $url_delete; ?>" class="btn btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                        </div>
                        
                        <div class="visible-xs visible-sm hidden-md hidden-lg">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo $url_view; ?>">
                                            <i class="clip clip-eye"></i> Ver
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo $url_edit; ?>">
                                            <i class="clip clip-pencil-3"></i> Editar
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo $url_delete; ?>">
                                            <i class="fa fa-times"></i> Eliminar
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
            }
        ?>
    
    </tbody>
</table>