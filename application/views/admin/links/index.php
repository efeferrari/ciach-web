
<div class='btn_container'>
    <a class="btn btn-green" href="/admin/links/crear">
        <i class="fa fa-plus"></i> Nuevo link
    </a>
</div>

<br><!-- Lindo parshe oe -->

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">#</th>
            <th class="hidden-xs">Autor</th>
            <th>Nombre</th>
            <th class="hidden-xs">Link</th>
            <th>Tipo</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    
        <?php
            foreach($links as $n => $link)
            {
                $url_view   = $this->config->item('base_url') . 'admin/links/ver/' . $link->id;
                $url_edit   = $this->config->item('base_url') . 'admin/links/editar/' . $link->id;
                $url_delete = $this->config->item('base_url') . 'admin/links/eliminar/' . $link->id;
                
                ?>
                <tr>
                    <td class="center"><?php echo $n+1; ?></td>
                    <td class="hidden-xs"><?php echo $link->autor; ?></td>
                    <td><?php echo $link->nombre; ?></td>
                    <td class="hidden-xs">
                        <a href='<?php echo $link->enlace; ?>' target='_blank' data-original-title="<?php echo $link->enlace; ?>" data-placement="bottom" class="btn btn-light-grey tooltips">
                            <i class='fa clip-link'></i>
                        </a>
                    </td>
                    <td><?php echo $this->tipo_enlace[$link->tipo]; ?></td>
                    <td><?php echo ($link->estado == 1)? '<span class="label label-success"> Publicada</span>' : '<span class="label label-warning"> Borrador</span>'; ?></td>
                    <td class="center">
                    
                        <div class="visible-md visible-lg hidden-sm hidden-xs">
                            <a href="<?php echo $url_view; ?>"   class="btn btn-green tooltips" data-placement="top" data-original-title="Ver"><i class="clip clip-eye"></i></a>
                            <a href="<?php echo $url_edit; ?>"   class="btn btn-blue tooltips" data-placement="top" data-original-title="Editar"><i class="clip clip-pencil-3"></i></a>
                            <a href="<?php echo $url_delete; ?>" class="btn btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                        </div>
                        
                        <div class="visible-xs visible-sm hidden-md hidden-lg">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo $url_view; ?>">
                                            <i class="clip clip-eye"></i> Ver
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo $url_edit; ?>">
                                            <i class="clip clip-pencil-3"></i> Editar
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo $url_delete; ?>">
                                            <i class="fa fa-times"></i> Eliminar
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
            }
        ?>
    
    </tbody>
</table>