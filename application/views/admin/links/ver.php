<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-newspaper-o"></i>Detalle
        
        <div class="panel-tools">
            
            
                <?php
                
                    if($link->estado == 0)
                    {
                        ?>
                        <button type="button" class="btn btn-gray btn-xs">
                            Borrador
                        </button>
                        <?php
                    }
                ?>
            
            <a href="/admin/links/editar/<?php echo $link->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-blue btn-xs">
                    Editar
                </button></a>
            
            <a href="/admin/links/eliminar/<?php echo $link->id; ?>" data-placement="top" data-original-title="Editar">
                <button type="button" class="btn btn-red btn-xs">
                    Eliminar
                </button></a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="2"><?php echo $link->nombre; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        Enlace
                    </td>
                    <td>
                        <?php
                        echo $link->enlace;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tipo
                    </td>
                    <td>
                        <?php
                        echo $this->tipo_enlace[ $link->tipo ];
                        ?>
                    </td>
                </tr>
                
                <?php
                
                    if($link->tipo == 1)
                    {
                        ?>
                            <tr>
                                <td>
                                    Imagen
                                </td>
                                <td>
                                    <?php
                                    
                                        if(strlen($link->imagen) > 0 )
                                        {
                                            ?><img src='<?php echo '/uploads/img/links/' . $link->imagen; ?>' alt='Imagen no disponible' style='max-width:30%;'><?php
                                        } else
                                        {
                                            echo 'No se adjunt&oacute; imagen';
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                    }
                
                ?>
                
                <tr>
                    <td nowrap>
                        Creado por
                    </td>
                    <td>
                        <?php
                        echo $link->autor;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a target='_blank' class="btn btn-green" role="button" href="<?php echo $link->enlace; ?>">Validar link</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>