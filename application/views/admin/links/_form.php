<?php

echo form_open_multipart('', array('class' => 'form-horizontal', 'id' => 'link_form'));

?>
<div class="form-group">
    <label class="col-sm-2 control-label" for="link_titulo">Nombre</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'link[nombre]', 'id' => 'link_nombre', 'class' => 'form-control', 'value' => isset($link)? $link->nombre : '', 'aria-required' => 'true')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="link_subtitulo">Enlace</label>
    <div class="col-sm-9">
        <?php echo form_input(array('name' => 'link[enlace]', 'id' => 'link_enlace', 'class' => 'form-control', 'value' => isset($link)? $link->enlace : '')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="link_subtitulo">Tipo de enlace</label>
    <div class="col-sm-9">
        <?php
            echo form_dropdown('link[tipo]', $this->tipo_enlace, (isset($link))? $link->tipo : 0, 'id="link_tipo" class="form-control"');
        ?>
    </div>
</div>

<div class="form-group" id='img_container'>
    <label class="col-sm-2 control-label" for="link_imagen">Imagen carrousel</label>
    <div class="col-sm-9">
        <?php echo form_upload(array('name' => 'imagen', 'id' => 'link_imagen', 'class' => 'file', 'data-show-preview' => 'false')); ?>
        
        <?php
            if($this->router->fetch_method() != 'crear' && isset($link))
            {
                if(strlen($link->imagen) > 0)
                {
                    ?>
                        <a data-toggle="modal" class="btn btn-info btn-xs" role="button" href="#modal_imagen_actual">Ver imagen actual</a>
                        
                        <div class="modal fade" id="modal_imagen_actual" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title"><?php echo $link->imagen; ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <img style='width:100%;' src='/uploads/img/links/<?php echo $link->imagen; ?>'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <label class="checkbox-inline">
                            <input name="link[_eliminar_imagen]" type="checkbox" class="red" value="<?php echo $link->imagen; ?>">
                            &iquest;Eliminar imagen?
                        </label>
                    <?php
                }
            }
        ?>
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-9">
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'link[estado]', 'id' => 'link_estado_on', 'value' => 1, 'checked' => (isset($link) && $link->estado == 1)? true : false, 'class' => 'square-green')); ?>
            Publicada
        </label>
        <label class="radio-inline">
            <?php echo form_radio(array('name' => 'link[estado]', 'id' => 'link_estado_off', 'value' => 0, 'checked' => (isset($link) && $link->estado == 0)? true : (!isset($link)? true : false), 'class' => 'square-red')); ?>
            Borrador
        </label>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-9">
        <a class="btn btn-primary" href="/admin/links">Cancelar</a>
        <?php
            echo form_submit(array('class' => 'btn btn-green', 'value' => 'Guardar'));
        ?>
    </div>
</div>

<div class='hidden_data'>
    <?php
    
    if($this->router->fetch_method() != 'crear')
    {
        echo form_hidden(array('link[id]' => isset($link)? $link->id : ''));
    } else
    {
        echo form_hidden(array('link[autor]' => $this->session->userdata('ciach')['id'] ));
    }
    
    ?>
</div>
<?php

echo form_close();



