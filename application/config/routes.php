<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'public/noticias/index';
$route['404_override']       = '';

# Errores controlados
$route['404'] = 'common/tools/not_found';
$route['403'] = 'common/tools/forbidden';

# Backend
    $route['admin']                  = 'admin/noticias';
    
    # Noticias
    $route['admin/noticias']               = 'admin/noticias/index';
    $route['admin/noticias/pagina/(:num)'] = 'admin/noticias/index/$1';
    
    $route['admin/bolsa-de-trabajo'] = 'admin/ofertas/index';
    $route['admin/links']            = 'admin/links/index';
    $route['admin/filiales']         = 'admin/filiales/index';
    $route['admin/seminarios']       = 'admin/seminarios/index';

    # Login
    $route['entrar']   = 'common/login/index';
    $route['salir']    = 'common/login/logout';
    $route['do-login'] = 'common/login/do_login';

# Public
    # Noticias
    $route['noticias']               = 'public/noticias/index';
    $route['noticias/pagina/(:num)'] = 'public/noticias/index/$1';
    $route['noticias/(:num)-(:any)'] = 'public/noticias/leer/$1/$2';
    
    # Colegiarse (registro p�blico)
    $route['colegiarse']      = 'public/colegiarse/index';
    $route['colegiarse/save'] = 'public/colegiarse/save';
    
    # Trabajos y pr�cticas (Bolsa de empleo)
    $route['trabajos-practicas']               = 'public/ofertas/index';
    $route['trabajos-practicas/pagina/(:num)'] = 'public/ofertas/index/$1';
    $route['trabajos-practicas/(:num)-(:any)'] = 'public/ofertas/leer/$1/$2';
    
    # Buscador de colegiados
    $route['colegiados']        = 'public/colegiados/index';
    $route['colegiados/buscar'] = 'public/colegiados/ajax_search';
    
    # Links
    $route['universidades'] = 'public/links/universidades';
    
    # Seminarios
    $route['seminarios'] = 'public/seminarios';
    
    # Est�ticos
    $route['mejora-tu-cv']          = 'public/_estaticos/mejora_cv';
    $route['el-insigne']            = 'public/_estaticos/el_insigne';
    $route['filiales']              = 'public/_estaticos/filiales';
    $route['estatuto-codigo-etica'] = 'public/_estaticos/estatuto_codigo_etica';
    $route['nosotros']              = 'public/_estaticos/nosotros';
    $route['contacto']              = 'public/_estaticos/contacto';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
