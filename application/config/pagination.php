<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

# Pagination
$config['uri_segment']          = 3;
$config['num_links']            = 5;
$config['per_page']             = 10;
$config['use_page_numbers']     = true;
$config['page_query_string']    = false;
$config['query_string_segment'] = '';

# Enclosing Markup
$config['full_tag_open'] = '';
$config['full_tag_close'] = ' ';

# First
$config['first_link'] = '<< Primero';
$config['first_tag_open'] = ' ';
$config['first_tag_close'] = '';

# Last
$config['last_link'] = '&Uacute;ltimo >>';
$config['last_tag_open'] = ' ';
$config['last_tag_close'] = '';

# Next
$config['next_link'] = 'Siguiente &gt;';
$config['next_tag_open'] = ' ';
$config['next_tag_close'] = '';

# Previus
$config['prev_link'] = '&lt; Primero';
$config['prev_tag_open'] = ' ';
$config['prev_tag_close'] = '';

# Current page
$config['cur_tag_open'] = ' <strong>';
$config['cur_tag_close'] = '</strong>';

# Digit Link
$config['num_tag_open'] = ' ';
$config['num_tag_close'] = '';

# Hide pages
# If you wanted to not list the specific pages (for example, you only want "next" and "previous" links), you can suppress their rendering by adding:
$config['display_pages'] = true;


/* End of file pagination.php */
/* Location: ./application/config/pagination.php */