<?php

class Password_Helper
{
    public static function generate_password($string)
    {
        return self::iterate_string($string);
    }
    
    public static function validate_password($pass, $db_pass)
    {
        if(self::iterate_string($pass) == $db_pass)
        {
            return true;
        }
        
        return false;
    }
    
    public static function iterate_string($string)
    {
        $CI =& get_instance();
        $CI->load->helper('security');
        
        for($i = 0; $i < 300; $i++)
        {
            $string = do_hash(do_hash(do_hash(do_hash(do_hash($string)))));
        }
        
        return $string;
    }
    
}
