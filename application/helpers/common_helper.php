<?php

# M�todos comunes

/**
 * Recibe una fecha en formato Y-m-d H:i:s de MySql y lo devolver� en el formato solicitado
 */
function reformat_date($date, $format = 'd-m-Y')
{
    $date = new DateTime($date);
    
    return $date->format($format);
}

function fix_format_date($date, $format = 'd-m-Y', $new_format = 'Y-m-d')
{
    return DateTime::createFromFormat($format, $date)->format($new_format);
}

/**
 * Slug
 */
function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text))
    {
        return 'n-a';
    }

    return $text;
}

function printr($algo)
{
    echo '<pre>' . print_r($algo, true) . '</pre>';
}

function plain_text($string)
{
    return substr(strip_tags($string), 0, 170);
}

/**
 * Convierte fecha en formato "Y-m-d H:i:s" a diff humanizado
 * Necesita tener instanciado el helper 'date'
 * $this->load->helper('date');
 * 
 */
function diff_for_human($fetcha)
{
    return timespan(strtotime($fetcha), now());
}

function upload_file($input_name = null, $config_data)
{
    $CI =& get_instance();
    $CI->load->library('upload', $config_data);
    
    $result = array();
    
    if (!$CI->upload->do_upload($input_name))
    {
        $result['status'] = false;
        $result['result'] = $CI->upload->display_errors();
        
    } else
    {
        $result['status'] = true;
        $result['result'] = $CI->upload->data();
    }
    
    return $result;
}

function delete_file($file)
{
    if(is_file($file))
    {
        unlink($file);
    }
}
