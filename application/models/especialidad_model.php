<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Especialidad_model extends CI_Model
{
    var $_table = 'especialidades';
    
	public function obtenerTodas()
	{
        return $this
            ->db
            ->select('e.*')
            ->from($this->_table . ' e')
            ->order_by('e.nombre', 'asc')
            ->get()
            ->result()
        ;
	}
}

