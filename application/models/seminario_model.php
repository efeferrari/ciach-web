<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seminario_model extends CI_Model
{
    var $_table = 'seminario';
    
	public function obtenerTodos($cantidad = 10, $pagina = 1)
	{
        return $this
            ->db
            ->select('l.*, u.nombre autor')
            ->from($this->_table . ' l')
            ->join('usuario u', 'l.autor = u.id', 'left')
            ->order_by('l.id', 'desc')
            ->limit($cantidad, (($pagina-1)* $cantidad))
            ->get()
            ->result()
        ;
	}
    
    public function obtener_por_parametros($params = array(), $cantidad = 10, $pagina = 1)
    {
        $data = $this
            ->db
            ->select('l.*, u.nombre autor')
            ->from($this->_table . ' l')
            ->join('usuario u', 'l.autor = u.id', 'left')
        ;
        
        if(count($params) > 0)
        {
            foreach($params as $key => $value)
            {
                $data = $data->where('l.' . $key, $value);
            }
        }
        
        return $data
            ->order_by('l.nombre', 'asc')
            ->limit($cantidad, (($pagina-1)* $cantidad))
            ->get()
            ->result()
        ;
    }
    
    public function obtenerActivos($cantidad = 10, $pagina = 1)
	{
        return $this
            ->db
            ->select('o.*, u.nombre autor')
            ->from($this->_table . ' o')
            ->join('usuario u', 'o.autor = u.id', 'left')
            ->where('o.estado', 1)
            ->order_by('o.id', 'desc')
            ->limit($cantidad, (($pagina-1)* $cantidad))
            ->get()
            ->result()
        ;
	}
    
    public function obtenerPorId($id = null)
	{
        if(is_null($id))
            show_404();
            
        return $this
            ->db
            ->select('l.*, u.nombre autor')
            ->from($this->_table . ' l')
            ->join('usuario u', 'l.autor = u.id', 'left')
            ->where('l.id', $id)
            ->get()
            ->row()
        ;
	}
    
    public function crear($data)
    {
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        $data['fecha_edicion']  = date('Y-m-d H:i:s');
        
        $this->db->insert($this->_table, $data);
        
        return $this->db->insert_id();
    }
    
    public function editar($id, $data)
    {
        if(!is_numeric($id))
            show_404();
        
        $data['fecha_edicion'] = date('Y-m-d H:i:s');
        
        $this->db->where('id', $id);
        
        try
        {
            $this->db->update($this->_table, $data);
            
        } catch(Exception $e)
        {
            return $e;
        }
        
        return true;
    }
    
    public function eliminar($id)
    {
        return $this->db->delete($this->_table, array('id' => $id));
    }
}

