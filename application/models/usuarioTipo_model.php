<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsuarioTipo_model extends CI_Model
{
    var $_table = 'usuario_tipo';
    
	public function obtenerTodos()
	{
        return $this
            ->db
            ->select('ut.*')
            ->from($this->_table . ' ut')
            ->order_by('ut.nombre', 'asc')
            ->get()
            ->result()
        ;
	}
    
    public function obtenerPorId($id = null)
	{
        if(is_null($id))
            show_404();
            
        return $this
            ->db
            ->select('ut.*')
            ->from($this->_table . ' ut')
            ->where('ut.id', $id)
            ->get()
            ->row()
        ;
	}
}

