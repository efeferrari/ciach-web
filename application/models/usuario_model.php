<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model
{
    var $_table = 'usuario';
    
	public function obtenerTodos($cantidad = 10, $pagina = 1)
	{
        return $this
            ->db
            ->select('u.*, ut.id usuario_tipo_id, ut.nombre usuario_tipo_nombre, ut.nombre tipo')
            ->from($this->_table . ' u')
            ->join('usuario_tipo ut', 'u.tipo = ut.id', 'inner')
            ->order_by('u.id', 'desc')
            ->limit($cantidad, (($pagina-1)* $cantidad))
            ->get()
            ->result()
        ;
	}
    
    public function obtenerPorId($id = null)
	{
        if(is_null($id))
            show_404();
            
        return $this
            ->db
            ->select('u.*, ut.id usuario_tipo_id, ut.nombre usuario_tipo_nombre, ut.nombre tipo, u.tipo tipo_id')
            ->from($this->_table . ' u')
            ->join('usuario_tipo ut', 'u.tipo = ut.id', 'inner')
            ->where('u.id', $id)
            ->get()
            ->row()
        ;
	}
    
    public function login($correo = null)
	{
        if(is_null($correo))
            show_404();
            
        return $this
            ->db
            ->select('u.*, ut.id usuario_tipo_id, ut.nombre usuario_tipo_nombre')
            ->from($this->_table . ' u')
            ->join('usuario_tipo ut', 'u.tipo = ut.id', 'inner')
            ->where('u.correo', $correo)
            ->get()
            ->row()
        ;
	}
    
    public function crear($data)
    {
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        $data['fecha_edicion']  = date('Y-m-d H:i:s');
        
        $this->db->insert($this->_table, $data);
        
        return $this->db->insert_id();
    }
    
    public function editar($id, $data)
    {
        if(!is_numeric($id))
            show_404();
        
        $data['fecha_edicion'] = date('Y-m-d H:i:s');
        
        $this->db->where('id', $id);
        
        try
        {
            $this->db->update($this->_table, $data);
            
        } catch(Exception $e)
        {
            return $e;
        }
        
        return true;
    }
    
    public function eliminar($id)
    {
        return $this->db->delete($this->_table, array('id' => $id));
    }
}

