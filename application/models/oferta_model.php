<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oferta_model extends CI_Model
{
    var $_table = 'oferta';
    
	public function obtenerTodas($cantidad = 10, $pagina = 1)
	{
        return $this
            ->db
            ->select('o.*, u.nombre autor')
            ->from($this->_table . ' o')
            ->join('usuario u', 'o.autor = u.id', 'left')
            ->order_by('o.id', 'desc')
            ->limit($cantidad, (($pagina-1)* $cantidad))
            ->get()
            ->result()
        ;
	}
    
    public function obtenerActivas($cantidad = 10, $pagina = 1)
	{
        return $this
            ->db
            ->select('o.*, u.nombre autor')
            ->from($this->_table . ' o')
            ->join('usuario u', 'o.autor = u.id', 'left')
            ->where('o.estado', 1)
            ->order_by('o.id', 'desc')
            ->limit($cantidad, (($pagina-1)* $cantidad))
            ->get()
            ->result()
        ;
	}
    
    public function obtenerPorId($id = null)
	{
        if(is_null($id))
            show_404();
            
        return $this
            ->db
            ->select('o.*, u.nombre autor')
            ->from($this->_table . ' o')
            ->join('usuario u', 'o.autor = u.id', 'left')
            ->where('o.id', $id)
            ->get()
            ->row()
        ;
	}
    
    public function obtenerAnteriorSiguiente($id = null)
	{
        if(is_null($id))
            show_404();
        
        $return = array();
        
        $return['anterior'] = $this
            ->db
            ->select('n.id, n.titulo')
            ->from($this->_table . ' n')
            ->join('usuario u', 'n.autor = u.id', 'left')
            ->where('n.id <', $id)
            ->where('n.estado', 1)
            ->order_by('n.id', 'desc')
            ->limit(1)
            ->get()
            ->row()
        ;
        
        $return['siguiente'] = $this
            ->db
            ->select('n.id, n.titulo')
            ->from($this->_table . ' n')
            ->join('usuario u', 'n.autor = u.id', 'left')
            ->where('n.id >', $id)
            ->where('n.estado', 1)
            ->order_by('n.id', 'desc')
            ->limit(1)
            ->get()
            ->row()
        ;
        
        return $return;
	}
    
    public function crear($data)
    {
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        $data['fecha_edicion']  = date('Y-m-d H:i:s');
        
        $this->db->insert($this->_table, $data);
        
        return $this->db->insert_id();
    }
    
    public function editar($id, $data)
    {
        if(!is_numeric($id))
            show_404();
        
        $data['fecha_edicion'] = date('Y-m-d H:i:s');
        
        $this->db->where('id', $id);
        
        try
        {
            $this->db->update($this->_table, $data);
            
        } catch(Exception $e)
        {
            return $e;
        }
        
        return true;
    }
    
    public function eliminar($id)
    {
        return $this->db->delete($this->_table, array('id' => $id));
    }
    
    public function totalActivas()
    {
        return $this
            ->db
            ->from($this->_table)
            ->where('estado', 1)
            ->count_all_results()
        ;
    }
}

