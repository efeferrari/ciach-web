<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Links extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('link_model');
    }
    
	public function universidades()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $params = array(
            'tipo'   => 2,
            'estado' => 1,
        );
        
        $data['links'] = $this->link_model->obtener_por_parametros($params, 300);
        
        #print_r($data['links']);die;
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/universidades', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    
}
