<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seminarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('seminario_model');
    }
    
	public function index($page = 1)
	{
        $config = get_config();
        $config['base_url'] = '/trabajos-practicas/pagina/';
        $config['total_rows'] = intval($this->seminario_model->obtenerActivos());
        
        $this->pagination->initialize($config);
        
        $data['seminarios'] = $this->seminario_model->obtenerActivos($this->pagination->per_page, $page);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/seminarios', $data);
        $this->load->view('public/layout/footer',  $data);
	}
}
