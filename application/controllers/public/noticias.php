<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('noticia_model');
    }
    
	public function index($page = 1)
	{
        $config = get_config();
        $config['base_url'] = '/noticias/pagina/';
        $config['total_rows'] = intval($this->noticia_model->totalActivas());
        
        $this->pagination->initialize($config);
        
        $data['noticias'] = $this->noticia_model->obtenerActivas($this->pagination->per_page, $page);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/noticias/index', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function leer($id = null, $slug = null)
    {
        $data['noticia']   = $this->noticia_model->obtenerPorId($id);
        $data['prev_next'] = $this->noticia_model->obtenerAnteriorSiguiente($id);
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/noticias/single', $data);
        $this->load->view('public/layout/footer',  $data);
    }
}
