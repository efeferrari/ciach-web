<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('oferta_model');
    }
    
	public function index($page = 1)
	{
        $config = get_config();
        $config['base_url'] = '/trabajos-practicas/pagina/';
        $config['total_rows'] = intval($this->oferta_model->totalActivas());
        
        $this->pagination->initialize($config);
        
        $data['ofertas'] = $this->oferta_model->obtenerActivas($this->pagination->per_page, $page);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/ofertas/index', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function leer($id = null, $slug = null)
    {
        $data['oferta']   = $this->oferta_model->obtenerPorId($id);
        $data['prev_next'] = $this->oferta_model->obtenerAnteriorSiguiente($id);
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/ofertas/single', $data);
        $this->load->view('public/layout/footer',  $data);
    }
}
