<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Colegiados extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('especialidad_model');
    }
    
	public function index($page = 1)
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $data['especialidades']      = $this->especialidad_model->obtenerTodas();
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/colegiados/index', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function ajax_search()
    {
        $post   = $this->input->post('buscador');
        $buscar = [];
        
        if(strlen($post['nombres']) > 0)
        {
            $buscar['usuario.nombre'] = $this->_prepare_string($post['nombres']);
        }
        
        if(strlen($post['ap_paterno']) > 0)
        {
            $buscar['info_colegiado.apellido_paterno'] = $this->_prepare_string($post['ap_paterno']);
        }
        
        if(strlen($post['ap_materno']) > 0)
        {
            $buscar['info_colegiado.apellido_paterno'] = $this->_prepare_string($post['ap_materno']);
        }
        
        if(strlen($post['especialidad_id']) > 0 && intval($post['especialidad_id']) > 0)
        {
            $buscar['info_colegiado.especialidad_id'] = $this->_prepare_string($post['especialidad_id']);
        }
        
        $query = '
            select
                usuario.id as usuario_id,
                CONCAT(usuario.nombre, " ", info_colegiado.apellido_paterno, " ", info_colegiado.apellido_materno) AS nombre_completo,
                usuario.correo,
                usuario.fecha_creacion,
                info_colegiado.telefono,
                info_colegiado.nombre_profesion,
                info_colegiado.universidad,
                especialidades.nombre as especialidad
            from usuario
            inner join info_colegiado
            on usuario.id = info_colegiado.usuario_id
            inner join especialidades
            on info_colegiado.especialidad_id = especialidades.id
            where usuario.tipo != 1
            ';
        
        $_tmp_query = [];
        foreach($buscar as $key => $value)
        {
            $_tmp_query[] = $key . ' like "' . $value . '"';
        }
        
        $query.= (count($_tmp_query) > 0)? ' and ' . join(' or ', $_tmp_query) : '';
        
        $result = $this->db->query($query);
        
        $data['colegiados'] = $result->result();
        $data['search_count'] = count($data['colegiados']);
        
        echo $this->load->view('public/colegiados/ajax_search', $data, true);
        die;
    }
    
    public function _prepare_string($str)
    {
        $buscar = [' ', 'á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú'];
        
        return '%' . str_replace($buscar, '%', $str) . '%';
    }
}
