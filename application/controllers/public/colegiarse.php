<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Colegiarse extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('usuario_model');
        $this->load->model('colegiado_model');
        $this->load->model('usuarioTipo_model');
        $this->load->model('especialidad_model');
        $this->load->helper('password');
    }
    
    public function index()
	{
        $data['breadcrum_titulo']    = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        $data['especialidades']      = $this->especialidad_model->obtenerTodas();
        $user_session                = $this->session->userdata('ciach');
        
        $this->load->view('public/layout/header',  $data);
        
        if(empty($user_session))
        {
            $this->load->view('public/colegiarse/registro', $data);
        } elseif(!empty($user_session) && $user_session['estado'] == 1)
        {
            //  La vista se llama "pagar", pero como se han demorado tantos a�os
            // en terminar el asunto, dejaron fuera la integraci�n de Transbank.
            //  Cuento corto, ac� s�lo hay un aviso de que usuario ya est�
            // registrado y que debe terminar manualmente el proceso de registro,
            // tal cual como en el sitio anterior
            $this->load->view('public/colegiarse/pagar', $data);
            
        } elseif(!empty($user_session) && $user_session['estado'] == 2)
        {
            $this->load->view('public/colegiarse/registro_completo', $data);
        }
        
        $this->load->view('public/layout/footer',  $data);
	}
    
	public function save()
	{
        $usuario   = $this->input->post('usuario');
        $colegiado = $this->input->post('colegiado');
        
        # Datos de usuario
        $usuario['tipo']   = 2;
        $usuario['estado'] = 1;
        $usuario['clave']  = Password_Helper::generate_password($usuario['clave']);
        
        try
        {
            $uid = $this->usuario_model->crear($usuario);
        
            if(intval($uid) > 0)
            {
                $colegiado['usuario_id']       = $uid;
                $colegiado['fecha_nacimiento'] = fix_format_date($colegiado['fecha_nacimiento']);
                $this->colegiado_model->crear($colegiado);
                
                $this->session->set_flashdata('alertas', array('success' => 'Registro exitoso. Puede iniciar sesi&oacute;n con el correo y claves indicados'));
                redirect('/entrar', 'refresh');
                die;
                
            } else
            {
                die('Registro err�neo');
            }
            
        } catch(Exception $e)
        {
            echo 'Error de registro: ' . $e->getMessage() . '<br>';
        }
	}
    
}
