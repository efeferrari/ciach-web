<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class _Estaticos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
	public function mejora_cv()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/mejora_cv', $data);
        $this->load->view('public/layout/footer',  $data);
	}
        
    public function seminarios()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/seminarios', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function el_insigne()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/el_insigne', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function filiales()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/filiales', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function estatuto_codigo_etica()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/estatuto_codigo_etica', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function nosotros()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/nosotros', $data);
        $this->load->view('public/layout/footer',  $data);
	}
    
    public function contacto()
	{
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('public/layout/header',  $data);
        $this->load->view('public/contacto', $data);
        $this->load->view('public/layout/footer',  $data);
	}
}
