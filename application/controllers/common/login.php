<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function index()
	{
		$data = array();
        
        $data['title'] = 'Identif&iacute;cate :: ' . $this->config->config['client_short_name'];
        $this->load->view('common/login', $data);
	}
    
    public function logout()
    {
        $this->session->unset_userdata('ciach');
        
        $this->session->set_flashdata('alertas', array('success' => 'Sesi&oacute;n finalizada'));
        
        redirect('entrar', 'refresh');
    }
    
    public function do_login()
    {
        if($this->input->post())
        {
            $this->load->model('usuario_model', 'mUsuario');
            
            $usuario = $this->mUsuario->login($this->input->post('username'));
            
            # Usuario existe
            if(isset($usuario->id))
            {
                # Validar contrase�a s�lo si usuario est� activo
                if($usuario->estado == 1)
                {
                    $this->load->helper('Password');
                    
                    if(Password_Helper::validate_password($this->input->post('password'), $usuario->clave) === true)
                    {
                        # Guardar data de usuario en session
                        $this->session->set_userdata(array('ciach' => (array)$usuario));
                        
                        $this->session->set_flashdata('alertas', array('success' => 'Identificaci&oacute;n correcta'));
                        
                        if($usuario->tipo == 1)
                        {
                            redirect('/admin/noticias', 'refresh');
                        } else
                        {
                            redirect('/', 'refresh');
                        }
                    } else
                    {
                        $this->session->set_flashdata('alertas', array('error' => 'Los datos ingresados no son correctos'));
                    }
                } else
                {
                    $this->session->set_flashdata('alertas', array('warning' => 'El usuario no se encuentra activo'));
                }
                
            } else
            {
                $this->session->set_flashdata('alertas', array('error' => 'El usuario no existe'));
            }
        }
        
        redirect('/entrar', 'refresh');
        die;
    }
}


