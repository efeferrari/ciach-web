<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends CI_Controller
{
    /**
     * No encontrado
     */
	public function not_found()
	{
        set_status_header(404);
        $this->load->view('common/404');
	}
    
    /**
     * Prohibido
     */
	public function forbidden()
	{
        set_status_header(403);
        $this->load->view('common/403');
	}
}
