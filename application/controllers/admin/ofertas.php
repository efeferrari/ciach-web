<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('oferta_model');
        $this->load->helper('form');
    }
    
	public function index()
	{
        $data['ofertas'] = $this->oferta_model->obtenerTodas($this->pagination->per_page);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/ofertas/index', $data);
        $this->load->view('admin/_layout/footer',  $data);
	}
    
    public function ver($id)
    {
        $data['oferta'] = $this->oferta_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Ver';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/ofertas/ver',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function eliminar($id)
    {
        if($this->input->post())
        {
            $this->oferta_model->eliminar($id);
            
            $this->session->set_flashdata('alertas', array('success' => 'Enlace eliminado correctamente.'));
            
            redirect('/admin/ofertas', 'refresh');
        }
        
        $data['oferta'] = $this->oferta_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Eliminar';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/ofertas/eliminar',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function crear()
    {
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Nueva';
        
        # Si viene request post, pasar par�metros a m�todo save()
        if($this->input->post())
        {
            try
            {
                $nuevo_id = $this->guardar($this->input->post('oferta'));
            } catch(Exception $e)
            {
                // echo $e->getMessage();die;
            }
            
            if(intval($nuevo_id) > 0)
            {
                redirect('/admin/ofertas/ver/' . $nuevo_id, 'refresh');
                
            } else
            {
                foreach($this->input->post('oferta') as $k => $v)
                {
                    $data['oferta'][$k] = $v;
                }
            }
        }
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/ofertas/crear', $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function editar($id)
    {
        $data['oferta'] = $this->oferta_model->obtenerPorId($id);
        
        if($this->input->post())
        {
            if($this->guardar($this->input->post('oferta')))
            {
                redirect('/admin/ofertas/ver/' . $id, 'refresh');
                
            } else
            {
                foreach($this->input->post('oferta') as $k => $v)
                {
                    $data['oferta'][$k] = $v;
                }
            }
        }
        
        $data['_controlador'] = __CLASS__;
        $data['_accion'] = 'Editar';
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Editar';
        
        $this->load->view('admin/_layout/header',   $data);
        $this->load->view('admin/ofertas/editar', $data);
        $this->load->view('admin/_layout/footer',   $data);
    }
    
    /**
     * Valida los datos del formulario y guarda
     * @todo
     */
    private function guardar($data)
    {
        $cfg = get_config();
        $cfg['upload']['upload_path'].= 'img/ofertas/logos/';
        $cfg['upload']['file_name']   = date('YmdHis') . slugify($data['titulo']);
        
        #printr($_FILES);die;
        
        try
        {
            # Me aburr� csm!! Lo valido directo del $_FILES ��
            if(isset($_FILES) && isset($_FILES['logo']) && strlen($_FILES['logo']['name']) > 0)
            {
                $upload = upload_file('logo', $cfg['upload']);
            }
            
            if(isset($data['_eliminar_logo']) && strlen($data['_eliminar_logo']) > 0)
            {
                delete_file($cfg['upload']['upload_path'] . $data['_eliminar_logo']);
                unset($data['_eliminar_logo']);
                $data['logo'] = null;
            }
            
        } catch(Exception $e)
        {
            die('catch: ' . $e->getMessage());
        }
        
        if(isset($upload))
        {
            if ($upload['status'] === false)
            {
                $this->session->set_flashdata('alertas', array('warning' => 'Subida de logo: ' . $this->upload->display_errors()));
            } else
            {
                $data['logo'] = $upload['result']['file_name'];
            }
        }
        
        if(isset($data['id']) && intval($data['id']) > 0)
        {
            return $this->oferta_model->editar($data['id'], $data);
        } else
        {
            return $this->oferta_model->crear($data);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */