<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Links extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->tipo_enlace = array(
            1 => 'Amigos',
            2 => 'Universidades',
            // 3 => utf8_encode('Links de inter�s'),
        );
        
        $this->load->model('link_model');
        $this->load->helper('form');
    }
    
	public function index()
	{
        $data['links'] = $this->link_model->obtenerTodas($this->pagination->per_page);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/links/index', $data);
        $this->load->view('admin/_layout/footer',  $data);
	}
    
    public function ver($id)
    {
        $data['link'] = $this->link_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Ver';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/links/ver',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function eliminar($id)
    {
        if($this->input->post())
        {
            $this->link_model->eliminar($id);
            
            $this->session->set_flashdata('alertas', array('success' => 'Enlace eliminado correctamente.'));
            
            redirect('/admin/links', 'refresh');
        }
        
        $data['link'] = $this->link_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Eliminar';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/links/eliminar',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function crear()
    {
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Nueva';
        
        # Si viene request post, pasar par�metros a m�todo save()
        if($this->input->post())
        {
            try
            {
                $nuevo_id = $this->guardar($this->input->post('link'));
            } catch(Exception $e)
            {
                // echo $e->getMessage();die;
            }
            
            if(intval($nuevo_id) > 0)
            {
                redirect('/admin/links/ver/' . $nuevo_id, 'refresh');
                
            } else
            {
                foreach($this->input->post('link') as $k => $v)
                {
                    $data['link'][$k] = $v;
                }
            }
        }
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/links/crear', $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function editar($id)
    {
        $data['link'] = $this->link_model->obtenerPorId($id);
        
        if($this->input->post())
        {
            if($this->guardar($this->input->post('link')))
            {
                redirect('/admin/links/ver/' . $id, 'refresh');
                
            } else
            {
                foreach($this->input->post('link') as $k => $v)
                {
                    $data['link'][$k] = $v;
                }
            }
        }
        
        $data['_controlador'] = __CLASS__;
        $data['_accion'] = 'Editar';
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Editar';
        
        $this->load->view('admin/_layout/header',   $data);
        $this->load->view('admin/links/editar', $data);
        $this->load->view('admin/_layout/footer',   $data);
    }
    
    /**
     * Valida los datos del formulario y guarda
     * @todo
     */
    private function guardar($data)
    {
        $cfg = get_config();
        $cfg['upload']['upload_path'].= 'img/links/';
        $cfg['upload']['file_name']   = date('YmdHis') . slugify($data['nombre']);
        
        try
        {
            # Me aburr� csm!! Lo valido directo del $_FILES ��
            if(isset($_FILES) && isset($_FILES['imagen']) && strlen($_FILES['imagen']['name']) > 0)
            {
                $upload = upload_file('imagen', $cfg['upload']);
            }
            
            if(isset($data['_eliminar_imagen']) && strlen($data['_eliminar_imagen']) > 0)
            {
                delete_file($cfg['upload']['upload_path'] . $data['_eliminar_imagen']);
                unset($data['_eliminar_imagen']);
                $data['imagen'] = null;
            }
            
        } catch(Exception $e)
        {
            die('catch: ' . $e->getMessage());
        }
        
        if(isset($upload))
        {
            if ($upload['status'] === false)
            {
                $this->session->set_flashdata('alertas', array('warning' => 'Subida de imagen destacada: ' . $this->upload->display_errors()));
            } else
            {
                $data['imagen'] = $upload['result']['file_name'];
            }
        }
        
        if(isset($data['id']) && intval($data['id']) > 0)
        {
            return $this->link_model->editar($data['id'], $data);
        } else
        {
            return $this->link_model->crear($data);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */