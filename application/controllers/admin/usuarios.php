<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('usuario_model');
        $this->load->model('usuarioTipo_model');
        $this->load->helper('form');
        $this->load->helper('password');
        
        $this->tipos_usuario = array();
        foreach($this->usuarioTipo_model->obtenerTodos() as $tipo)
        {
            $this->tipos_usuario[$tipo->id] = $tipo->nombre;
        }
    }
    
	public function index()
	{
        $data['usuarios'] = $this->usuario_model->obtenerTodos($this->pagination->per_page);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/usuarios/index', $data);
        $this->load->view('admin/_layout/footer',  $data);
	}
    
    public function ver($id)
    {
        $data['usuario'] = $this->usuario_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Ver';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/usuarios/ver',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function eliminar($id)
    {
        if($this->input->post())
        {
            $this->usuario_model->eliminar($id);
            
            $this->session->set_flashdata('alertas', array('success' => 'El usuario fue eliminado correctamente.'));
            
            redirect('/admin/usuarios', 'refresh');
        }
        
        $data['usuario'] = $this->usuario_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Eliminar';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/usuarios/eliminar',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function crear()
    {
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Nuevo';
        
        # Si viene request post, pasar par�metros a m�todo save()
        if($this->input->post())
        {
            $nuevo_id = $this->guardar($this->input->post('usuario'));
            
            if(intval($nuevo_id) > 0)
            {
                redirect('/admin/usuarios/ver/' . $nuevo_id, 'refresh');
                
            } else
            {
                foreach($this->input->post('usuario') as $k => $v)
                {
                    $data['usuario'][$k] = $v;
                }
            }
        }
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/usuarios/crear', $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function editar($id)
    {
        $data['usuario'] = $this->usuario_model->obtenerPorId($id);
        
        if($this->input->post())
        {
            if($this->guardar($this->input->post('usuario')))
            {
                redirect('/admin/usuarios/ver/' . $id, 'refresh');
                
            } else
            {
                foreach($this->input->post('usuario') as $k => $v)
                {
                    $data['usuario'][$k] = $v;
                }
            }
        }
        
        $data['_controlador'] = __CLASS__;
        $data['_accion'] = 'Editar';
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Editar';
        
        $this->load->view('admin/_layout/header',   $data);
        $this->load->view('admin/usuarios/editar', $data);
        $this->load->view('admin/_layout/footer',   $data);
    }
    
    /**
     * Valida los datos del formulario y guarda
     * @todo
     */
    private function guardar($data)
    {
        if(isset($data['clave']) && strlen($data['clave']) > 0)
        {
            $data['clave'] = Password_Helper::generate_password($data['clave']);
        }
        
        if(isset($data['id']) && intval($data['id']) > 0)
        {
            return $this->usuario_model->editar($data['id'], $data);
        } else
        {
            return $this->usuario_model->crear($data);
            
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */