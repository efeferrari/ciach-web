<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('noticia_model');
        $this->load->helper('form');
    }
    
	public function index($page = 1)
	{
        // $config = get_config();
        // $config['base_url'] = '/admin/noticias/pagina/';
        // $config['total_rows'] = intval($this->noticia_model->total());
        
        // $this->pagination->initialize($config);
        
        // Pongo ese 50 para no hacer el paginador xD
        $data['noticias'] = $this->noticia_model->obtenerTodas();
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/noticias/index', $data);
        $this->load->view('admin/_layout/footer',  $data);
	}
    
    public function ver($id)
    {
        $data['noticia'] = $this->noticia_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Ver';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/noticias/ver',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function eliminar($id)
    {
        if($this->input->post())
        {
            $this->noticia_model->eliminar($id);
            
            $this->session->set_flashdata('alertas', array('success' => 'La noticia fue eliminada correctamente.'));
            
            redirect('/admin/noticias', 'refresh');
        }
        
        $data['noticia'] = $this->noticia_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Eliminar';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/noticias/eliminar',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function crear()
    {
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Nueva';
        
        # Si viene request post, pasar par�metros a m�todo save()
        if($this->input->post())
        {
            try
            {
                $nuevo_id = $this->guardar($this->input->post('noticia'));
            } catch(Exception $e)
            {
                // echo $e->getMessage();die;
            }
            
            if(intval($nuevo_id) > 0)
            {
                redirect('/admin/noticias/ver/' . $nuevo_id, 'refresh');
                
            } else
            {
                foreach($this->input->post('noticia') as $k => $v)
                {
                    $data['noticia'][$k] = $v;
                }
            }
        }
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/noticias/crear', $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function editar($id)
    {
        $data['noticia'] = $this->noticia_model->obtenerPorId($id);
        
        if($this->input->post())
        {
            if($this->guardar($this->input->post('noticia')))
            {
                redirect('/admin/noticias/ver/' . $id, 'refresh');
                
            } else
            {
                foreach($this->input->post('noticia') as $k => $v)
                {
                    $data['noticia'][$k] = $v;
                }
            }
        }
        
        $data['_controlador'] = __CLASS__;
        $data['_accion'] = 'Editar';
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Editar';
        
        $this->load->view('admin/_layout/header',   $data);
        $this->load->view('admin/noticias/editar', $data);
        $this->load->view('admin/_layout/footer',   $data);
    }
    
    /**
     * Valida los datos del formulario y guarda
     * @todo
     */
    private function guardar($data)
    {
        $cfg = get_config();
        $cfg['upload']['upload_path'].= 'img/noticias/destacadas/';
        $cfg['upload']['file_name']   = date('YmdHis') . slugify($data['titulo']);
        
        try
        {
            # Me aburr� csm!! Lo valido directo del $_FILES ��
            if(isset($_FILES) && isset($_FILES['imagen']) && strlen($_FILES['imagen']['name']) > 0)
            {
                $upload = upload_file('imagen', $cfg['upload']);
            }
            
            if(isset($data['_eliminar_imagen']) && strlen($data['_eliminar_imagen']) > 0)
            {
                delete_file($cfg['upload']['upload_path'] . $data['_eliminar_imagen']);
                unset($data['_eliminar_imagen']);
                $data['imagen'] = null;
            }
            
        } catch(Exception $e)
        {
            //die('catch: ' . $e->getMessage());
        }
        
        if(isset($upload))
        {
            if ($upload['status'] === false)
            {
                $this->session->set_flashdata('alertas', array('warning' => 'Subida de imagen destacada: ' . $this->upload->display_errors()));
            } else
            {
                $data['imagen'] = $upload['result']['file_name'];
            }
        }
        
        if(isset($data['id']) && intval($data['id']) > 0)
        {
            return $this->noticia_model->editar($data['id'], $data);
        } else
        {
            return $this->noticia_model->crear($data);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */