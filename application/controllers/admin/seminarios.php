<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seminarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('seminario_model');
        $this->load->helper('form');
    }
    
	public function index()
	{
        $data['seminarios'] = $this->seminario_model->obtenerTodos($this->pagination->per_page);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = '';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/seminarios/index', $data);
        $this->load->view('admin/_layout/footer',  $data);
	}
    
    public function ver($id)
    {
        $data['seminario'] = $this->seminario_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Ver';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/seminarios/ver',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function eliminar($id)
    {
        if($this->input->post())
        {
            $sem = $this->seminario_model->obtenerPorId($id);
            
            $cfg = get_config();
            $cfg['upload']['upload_path'].= 'img/seminarios/';
            delete_file($cfg['upload']['upload_path'] . $sem->imagen);
            
            $this->seminario_model->eliminar($id);
            
            $this->session->set_flashdata('alertas', array('success' => 'Seminario eliminado correctamente.'));
            
            redirect('/admin/seminarios', 'refresh');
        }
        
        $data['seminario'] = $this->seminario_model->obtenerPorId($id);
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Eliminar';
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/seminarios/eliminar',   $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function crear()
    {
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Nuevo';
        
        # Si viene request post, pasar par�metros a m�todo save()
        if($this->input->post())
        {
            try
            {
                $nuevo_id = $this->guardar($this->input->post('seminario'));
            } catch(Exception $e)
            {
                // echo $e->getMessage();die;
            }
            
            if(intval($nuevo_id) > 0)
            {
                redirect('/admin/seminarios/ver/' . $nuevo_id, 'refresh');
                
            } else
            {
                foreach($this->input->post('seminario') as $k => $v)
                {
                    $data['seminario'][$k] = $v;
                }
            }
        }
        
        $this->load->view('admin/_layout/header',  $data);
        $this->load->view('admin/seminarios/crear', $data);
        $this->load->view('admin/_layout/footer',  $data);
    }
    
    public function editar($id)
    {
        $data['seminario'] = $this->seminario_model->obtenerPorId($id);
        
        if($this->input->post())
        {
            if($this->guardar($this->input->post('seminario')))
            {
                redirect('/admin/seminarios/ver/' . $id, 'refresh');
                
            } else
            {
                foreach($this->input->post('seminario') as $k => $v)
                {
                    $data['seminario'][$k] = $v;
                }
            }
        }
        
        $data['_controlador'] = __CLASS__;
        $data['_accion'] = 'Editar';
        
        $data['breadcrum_titulo'] = __CLASS__;
        $data['breadcrum_subtitulo'] = 'Editar';
        
        $this->load->view('admin/_layout/header',   $data);
        $this->load->view('admin/seminarios/editar', $data);
        $this->load->view('admin/_layout/footer',   $data);
    }
    
    /**
     * Valida los datos del formulario y guarda
     * @todo
     */
    private function guardar($data)
    {
        $cfg = get_config();
        $cfg['upload']['upload_path'].= 'img/seminarios/';
        $cfg['upload']['file_name']   = date('YmdHis') . slugify($data['titulo']);
        
        try
        {
            # Me aburr� csm!! Lo valido directo del $_FILES ��
            if(isset($_FILES) && isset($_FILES['imagen']) && strlen($_FILES['imagen']['name']) > 0)
            {
                $upload = upload_file('imagen', $cfg['upload']);
            }
            
            if(isset($data['_eliminar_imagen']) && strlen($data['_eliminar_imagen']) > 0)
            {
                delete_file($cfg['upload']['upload_path'] . $data['_eliminar_imagen']);
                unset($data['_eliminar_imagen']);
                $data['imagen'] = null;
            }
            
        } catch(Exception $e)
        {
            die('catch: ' . $e->getMessage());
        }
        
        if(isset($upload))
        {
            if ($upload['status'] === false)
            {
                $this->session->set_flashdata('alertas', array('warning' => 'Subida de imagen destacada: ' . $this->upload->display_errors()));
            } else
            {
                $data['imagen'] = $upload['result']['file_name'];
            }
        }
        
        if(isset($data['id']) && intval($data['id']) > 0)
        {
            return $this->seminario_model->editar($data['id'], $data);
        } else
        {
            return $this->seminario_model->crear($data);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */