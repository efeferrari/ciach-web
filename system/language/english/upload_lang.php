<?php


$lang['upload_userfile_not_set'] = "Unable to find a post variable called userfile.";
$lang['upload_file_exceeds_limit'] = "El tama�o del archivo es superior a lo permitido por el servidor.";
$lang['upload_file_exceeds_form_limit'] = "El tama�o del archivo es superior a lo permitido por la configuraci�n del formulario.";
$lang['upload_file_partial'] = "El archivo fue parcialmente subido.";
$lang['upload_no_temp_directory'] = "La carpeta temporal se perdi�.";
$lang['upload_unable_to_write_file'] = "No se puede guardar el archivo en el disco";
$lang['upload_stopped_by_extension'] = "Se detuvo la subida por la extensi�n del archivo.";
$lang['upload_no_file_selected'] = "No se elegi&oacute; archivo para subir.";
$lang['upload_invalid_filetype'] = "El tipo de archivo no est� permitido.";
$lang['upload_invalid_filesize'] = "El archivo excede el tama�o permitido.";
$lang['upload_invalid_dimensions'] = "La imagen que intentas subir excede el alto o ancho m�ximos permitidos.";
$lang['upload_destination_error'] = "Se produjo un problema moviendo el archivo a la carpeta definitiva.";
$lang['upload_no_filepath'] = "La carpeta de subida no parece ser v�lida.";
$lang['upload_no_file_types'] = "No se especiic� ning�n tipo de archivo permitido.";
$lang['upload_bad_filename'] = "El archivo que intentas subir ya existe en el servidor.";
$lang['upload_not_writable'] = "La carpeta de subida no parece tener permisos de escritura.";

/*
$lang['upload_userfile_not_set'] = "Unable to find a post variable called userfile.";
$lang['upload_file_exceeds_limit'] = "The uploaded file exceeds the maximum allowed size in your PHP configuration file.";
$lang['upload_file_exceeds_form_limit'] = "The uploaded file exceeds the maximum size allowed by the submission form.";
$lang['upload_file_partial'] = "The file was only partially uploaded.";
$lang['upload_no_temp_directory'] = "The temporary folder is missing.";
$lang['upload_unable_to_write_file'] = "The file could not be written to disk.";
$lang['upload_stopped_by_extension'] = "The file upload was stopped by extension.";
$lang['upload_no_file_selected'] = "You did not select a file to upload.";
$lang['upload_invalid_filetype'] = "The filetype you are attempting to upload is not allowed.";
$lang['upload_invalid_filesize'] = "The file you are attempting to upload is larger than the permitted size.";
$lang['upload_invalid_dimensions'] = "The image you are attempting to upload exceedes the maximum height or width.";
$lang['upload_destination_error'] = "A problem was encountered while attempting to move the uploaded file to the final destination.";
$lang['upload_no_filepath'] = "The upload path does not appear to be valid.";
$lang['upload_no_file_types'] = "You have not specified any allowed file types.";
$lang['upload_bad_filename'] = "The file name you submitted already exists on the server.";
$lang['upload_not_writable'] = "The upload destination folder does not appear to be writable.";
*/

/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */