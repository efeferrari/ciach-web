﻿
function is_empty(input)
{
    if(input.val().length == 0)
    {
        return true;
    }
    
    return false;
}

function add_error(str)
{
    $(str).parent().parent().addClass('has_error');
}

function remove_error(str)
{
    $(str).parent().parent().removeClass('has_error');
}


function scroll_to_id(id)
{
    $('html,body').animate({ scrollTop: $(id).offset().top - 40 }, 'slow');
    $(id).focus();
}
