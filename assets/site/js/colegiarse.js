﻿$(function()
{
    $.datepicker.regional['es'] =
    {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        changeMonth: true,
        changeYear: true
    };

    $.datepicker.setDefaults($.datepicker.regional['es']);
    
    var _max_date_ = new Date();
    _max_date_.setFullYear( (new Date()).getFullYear() - 18 );
    
    $('.has_calendar').datepicker({ maxDate: _max_date_ });
    
    $('#colegiarse').on('submit', function()
    {
        var submit = true;
        var _first = '';
        
        $.each($('#colegiarse input').not('#submit, #acepta'), function(k, _input)
        {
            var input = $(_input);
            if(is_empty(input))
            {
                submit = false;
                add_error('#' + input.attr('id'));
                
                if(_first == '')
                    _first = '#' + input.attr('id');
                
            } else
            {
                remove_error('#' + input.attr('id'));
            }
        });
        
        if ($('#acepta').is(':checked') == false)
        {
            submit = false;
            add_error('#acepta');
            if(_first == '')
                _first = '#acepta';
        }
        
        if(submit == true)
        {
            return true;
        }
        
        scroll_to_id(_first);
        return false;
    });
    
    $('#colegiarse input').on('change', function()
    {
        $(this).parent().parent().removeClass('has_error');
    });
});
